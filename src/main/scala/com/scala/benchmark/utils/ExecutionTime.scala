package com.scala.benchmark.utils

import java.time.LocalDateTime

object ExecutionTime extends App {

  def operationMeasurement[T](operation: => T): Long = {
    val time = System.currentTimeMillis()
    operation
    //println(operation.toString)
    System.currentTimeMillis() - time
  }

  def printThreadInfo(txt: String): Unit = {
    val thread = Thread.currentThread.getName
    println(s"${LocalDateTime.now} [$thread] $txt")
  }

  def time[T](operation: => T): (T, Long) = {
    val time = System.currentTimeMillis()
    val t = operation
    (t, System.currentTimeMillis() - time)
  }
}
