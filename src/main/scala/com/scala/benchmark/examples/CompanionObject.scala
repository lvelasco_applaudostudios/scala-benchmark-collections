package com.scala.benchmark.examples

object CompanionObject extends App {
  class Movie(val name: String, val year: Short)

  // An object that has the same name as a class is called a companion object of the class, and it is often used to contain factory methods for the class that it complements:

  //A companion object can also see private values and variables of the corresponding classes' instantiated objects

  object Movie {
    def academyAwardBestMoviesForYear(x: Short) = {
      //This is a match statement, more powerful than a Java switch statement!
      x match {
        case 1930 => Some(new Movie("All Quiet On the Western Front", 1930))
        case 1931 => Some(new Movie("Cimarron", 1931))
        case 1932 => Some(new Movie("Grand Hotel", 1932))
        case _ => None
      }
    }
  }

  println(Movie.academyAwardBestMoviesForYear(1932).get.name)



  class Person(
                val name: String,
                private val superheroName: String) //The superhero name is private!

  object Person {
    def showMeInnerSecret(x: Person) = x.superheroName
  }

  val clark = new Person("Clark Kent", "Superman")
  val peter = new Person("Peter Parker", "Spider-Man")

  println(Person.showMeInnerSecret(clark))
  println(Person.showMeInnerSecret(peter))
}
