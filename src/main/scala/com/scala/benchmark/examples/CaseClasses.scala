package com.scala.benchmark.examples

object CaseClasses extends App {
  abstract class Term
  case class Var(name: String) extends Term
  case class Fun(arg: String, body: Term) extends Term
  case class App(f: Term, v: Term) extends Term

  case class Person(first: String, last: String)

  val p1 = new Person("Fred", "Jones")
  val p2 = new Person("Shaggy", "Rogers")
  val p3 = new Person("Fred", "Jones")
  println(p1.hashCode())
  println((p1.hashCode == p2.hashCode))
  println((p1.hashCode == p3.hashCode))

  case class Dog(name: String, breed: String)

  val d1 = Dog("Scooby", "Doberman")
  val d2 = Dog("Rex", "Custom")
  val d3 = new Dog("Scooby", "Doberman")

  val d11 = Dog("Scooby", "Doberman")
  println(d11.toString)

  case class Person1(first: String, last: String, age: Int = 0, ssn: String = "")
  val p11 = Person1("Fred", "Jones", 23, "111-22-3333")

  val parts = Person1.unapply(p11).get
  println("as a tuple: "+parts._1+" | "+parts._2+" | "+parts._3+" | "+parts._4)


  // case classes are serializable
  case class PersonCC(firstName: String, lastName: String)
  val indy = PersonCC("Indiana", "Jones")

  println(indy.isInstanceOf[Serializable])

}
