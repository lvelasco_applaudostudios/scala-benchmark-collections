package com.scala.benchmark.examples

import java.math.BigInteger

object Implicits extends App {
  class KoanIntWrapper(val original: Int) {
    def isOdd = original % 2 != 0
  }

  implicit def thisMethodNameIsIrrelevant(value: Int) =
    new KoanIntWrapper(value)

  println(19.isOdd)
  println(20.isOdd)

  //Implicits rules can be imported into your scope with an import:
  object MyPredef {

    class KoanIntWrapper(val original: Int) {
      def isOdd = original % 2 != 0

      def isEven = !isOdd
    }

    implicit def thisMethodNameIsIrrelevant(value: Int) =
      new KoanIntWrapper(value)
  }

  //import MyPredef._
  //imported implicits come into effect within this scope
  println(19.isOdd)
  println(20.isOdd)


  //Implicits can be used to automatically convert a value's type to another
  implicit def Int2BigIntegerConvert(value: Int): BigInteger =
    new BigInteger(value.toString)

  def add(a: BigInteger, b: BigInteger) = a.add(b)

  println(add(Int2BigIntegerConvert(3), Int2BigIntegerConvert(6)) == Int2BigIntegerConvert(9))
  println((add(3, 6) == 9))
  println(add(3, 6) == Int2BigIntegerConvert(9))
  println(add(3,6) == (9: BigInteger))
  println(add(3,6).intValue == 9)

  //Implicits can be used to declare a value to be provided as a default as long as an implicit value is set with in the scope. These are called Implicit Function Parameters:
  def howMuchCanIMake_?(hours: Int)(implicit dollarsPerHour: BigDecimal) =
    dollarsPerHour * hours

  implicit val hourlyRate = BigDecimal(34)

  println(howMuchCanIMake_?(30))


  //Implicit Function Parameters can contain a list of implicits
  /*{
    def howMuchCanIMake(hours: Int)(implicit amount: BigDecimal, currencyName: String) =
      (amount * hours).toString() + " " + currencyName

    implicit val hourlyRate1 = BigDecimal(34)
    implicit val currencyName1 = "Dollars"

    println(howMuchCanIMake(30))
  }*/
}
