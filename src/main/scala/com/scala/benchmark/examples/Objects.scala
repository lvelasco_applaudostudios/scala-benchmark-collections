package com.scala.benchmark.examples

object Objects extends App {

  object Greeting {
    def english = "Hi"

    def espanol = "Hola"
  }

  val x = Greeting
  val y = x
  println(x)
  println(y)
  val z = Greeting
  println(z)
}
