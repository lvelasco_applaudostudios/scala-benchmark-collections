package com.scala.benchmark.examples

object InfixTypes extends App {
  case class Person(name: String)
  class Loves[A, B](val a: A, val b: B)

  def announceCouple(couple: Person Loves Person) =
  //Notice our type: Person loves Person!
    couple.a.name + " is in love with " + couple.b.name

  val romeo = new Person("Romeo")
  val juliet = new Person("Juliet")

  println(announceCouple(new Loves(romeo, juliet)))

  case class Person1(name: String) {
    def loves(person: Person1) = new Loves(this, person)
  }

  class Loves1[A, B](val a: A, val b: B)

  def announceCouple1(couple: Person1 Loves Person1) =
  //Notice our type: Person loves Person!
    couple.a.name + " is in love with " + couple.b.name

  val romeo1 = new Person1("Romeo")
  val juliet1 = new Person1("Juliet")

  println(announceCouple1(romeo1 loves juliet1))
}
