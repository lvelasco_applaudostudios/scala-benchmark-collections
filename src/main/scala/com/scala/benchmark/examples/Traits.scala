package com.scala.benchmark.examples

object Traits extends App {

  case class Event(name: String)

  trait EventListener {
    def listen(event: Event): String
  }

  class MyListener extends EventListener {
    def listen(event: Event): String = {
      event match {
        case Event("Moose Stampede") =>
          "An unfortunate moose stampede occurred"
        case _ => "Nothing of importance occurred"
      }
    }
  }

  val evt = Event("Moose Stampede")
  val myListener = new MyListener
  println(myListener.listen(evt))


  // A class can only extend from one class or trait, any subsequent extension should use the keyword with:
  class OurListener

  class MyListener1 extends OurListener with EventListener {
    def listen(event: Event): String = {
      event match {
        case Event("Woodchuck Stampede") =>
          "An unfortunate woodchuck stampede occurred"
        case _ => "Nothing of importance occurred"
      }
    }
  }
  val evt1 = Event("Woodchuck Stampede")
  val myListener1 = new MyListener1
  println(myListener.listen(evt1))


  // Traits are polymorphic. Any type can be referred to by another type if related by extension
  class MyListener2 extends EventListener {
    def listen(event: Event): String = {
      event match {
        case Event("Moose Stampede") =>
          "An unfortunate moose stampede occurred"
        case _ => "Nothing of importance occurred"
      }
    }
  }

  val myListener2 = new MyListener2
  println(myListener2.isInstanceOf[MyListener]) // true
  println(myListener2.isInstanceOf[EventListener]) // true
  println(myListener2.isInstanceOf[Any]) // true
  println(myListener2.isInstanceOf[AnyRef]) // true


  // Traits also can use self-types. A self-type lists the required dependencies for mixing in the trait. When mixing in the main trait, all self-type dependencies of that trait must also be mixed in, otherwise a compile-time error is thrown.
  //
  //Also, the dependencies can't have identical method/property names or else you'll get an illegal inheritance error.
  trait B {
    def bId = 2
  }

  trait A { self: B =>

    def aId = 1
  }

  //val a = new A  //***does not compile!!!***
  val obj = new A with B
  println((obj.aId + obj.bId))
}
