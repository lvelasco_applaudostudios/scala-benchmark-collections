package com.scala.benchmark.examples

object HighOrderFunctions extends App {

  def lambda = { x: Int => x + 1 }
  def lambda2 = (x: Int) => x + 2
  val lambda3 = (x: Int) => x + 3

  val lambda4 = new Function1[Int, Int] {
    def apply(v1: Int): Int = v1 - 1
  }

  def lambda5(x: Int) = x + 1

  val result = lambda(3)
  val result1andhalf = lambda.apply(3)

  val result2 = lambda2(3)
  val result3 = lambda3(3)
  val result4 = lambda4(3)
  val result5 = lambda5(3)

  println(result)
  println(result1andhalf)
  println(result2)
  println(result3)
  println(result4)
  println(result5)




  // A closure is a function which maintains a reference to one or more variables outside of the function scope
  // (it "closes over" the variables).
  // Scala will detect that you are using variables outside of scope and create an object instance to hold the
  // shared variables
  var incrementer = 1

  def closure = { x: Int => x + incrementer }

  val result1 = closure(10)
  println(11)

  incrementer = 2

  val result211 = closure(10)
  println(result211)



  def summation(x: Int, y: Int => Int) = y(x)

  var incrementer1 = 3
  def closure1 = (x: Int) => x + incrementer1

  val result111 = summation(10, closure1)
  println("resultado: "+result111)



  def addWithoutSyntaxSugar(x: Int): Function1[Int, Int] = {
    new Function1[Int, Int]() {
      def apply(y: Int): Int = x + y
    }
  }
  println(addWithoutSyntaxSugar(1).isInstanceOf[Function1[Int, Int]])
  println(addWithoutSyntaxSugar(1))
  println(addWithoutSyntaxSugar(2)(3))
  def fiveAdder: Function1[Int, Int] = addWithoutSyntaxSugar(5)
  println(fiveAdder(5))


  def addWithSyntaxSugar(x: Int) = (y: Int) => x + y
  println(addWithSyntaxSugar(1).isInstanceOf[Function1[Int, Int]])
  println(addWithSyntaxSugar(2)(3))
  def fiveAdder1 = addWithSyntaxSugar(5)
  println(fiveAdder1(5))

  def addWithSyntaxSugar1(x: Int) = (y: Int) => x + y
  println(addWithSyntaxSugar1(1).isInstanceOf[Function1[_, _]])






  def makeUpper(xs: List[String]) =
    xs map {
      _.toUpperCase
    }

  def makeWhatEverYouLike(xs: List[String], sideEffect: String => String) =
    xs map sideEffect

  println("makeUpper: "+makeUpper(List("abc", "xyz", "123")))

  println("makeWhatEverYouLike: "+makeWhatEverYouLike(List("ABC", "XYZ", "123"), x => x.toLowerCase))

  val myName = (name: String) => s"My name is $name"
  println("inline: " + makeWhatEverYouLike(List("John", "Mark"), myName))

  println("length: "+List("Scala", "Erlang", "Clojure").map(_.length))
}
