package com.scala.benchmark.examples

import scala.collection.mutable.ListBuffer

object HRPriceToSell extends App {

  def valuation(reqArea: Long, area: Array[Long], price: Array[Long]): Long = {
    //val res = 0L
    val areaPrice = area.zip(price)
    //val areaPriceAux = areaPrice
    //val areaPriceSize = areaPrice.size
    //area.foreach(println)
    /*println(area.foreach({ x =>

      if(x == 95849)
        println(x)

    }))*/

    //House(val area: Int, val price: Int, val compList: List[Int],
    //      val pm: Int, val o: Int, val priceMinusPm: Int, val threeMultO: Int, val isOutlier: Boolean)

    //var trivialCase = true
    var isBuyersHouseGreater = true

    val table = new ListBuffer[House]
    for(i <- 0 until areaPrice.size) {
      //println("------------- begin loop number: "+i+"---------------------")
      //println(area.filter(x => x == areaPrice(i)._1))
      val area = areaPrice(i)._1
      val price = areaPrice(i)._2
      val (compList, pm, o, priceMinusPm, threeMult0, isOutlier) = compListF(areaPrice(i)._1, areaPrice(i)._2, areaPrice)

      table += House(area = area,
                     price = price,
                     compList = compList, pm = pm, o = o, priceMinusPm = priceMinusPm, threeMultO = threeMult0, isOutlier = isOutlier)

      //println("-------------- end loop number: "+i+"--------------------")
    }

    println("+++++++++++++++++++ begin checking ListBuffer House: ")
    table.foreach(println)
    println("+++++++++++++++++++ end checking ListBuffer House: ")

    var houses = new ListBuffer[House]
    for(i <- 0 until table.size){

      /*if(table(i).compList.size > 0)
        trivialCase = false*/

      if(! table(i).isOutlier ){
        houses.addOne(table(i))
      }
    }

    println("------------------ begin checking houses ")

    for(i <- 0 until houses.size) {
      if(houses(i).area > reqArea  && isBuyersHouseGreater == true)
          isBuyersHouseGreater = false
    }
    houses.foreach(println)
    println("------------------ end checking houses ")

    println("reqArea: " + reqArea)

    var closestMaxVal = 0L
    var closestMinVal = 0L

    var closestMinPricePosition = 0
    var closestMaxPricePosition = 0

    var closestMinPrice = 0L
    var closestMaxPrice = 0L


    var answer:Float = 0

    var minArea = 0L
    var maxArea = 0L

    closestMaxVal = getClosestMax(reqArea, area)
    closestMinVal = getClosestMin(reqArea, area)
    println("closestMaxVal: " + closestMaxVal)
    println("closestMinVal: " + closestMinVal)

    closestMinPricePosition = area.indexOf(closestMinVal)
    closestMaxPricePosition = area.indexOf(closestMaxVal)
    println("indexOf ClosestMin: " + closestMinPricePosition)
    println("indexOf ClosestMax: " + closestMaxPricePosition)

    closestMinPrice = price(closestMinPricePosition)
    closestMaxPrice = price(closestMaxPricePosition)
    println("ClosestMinPrice: " + closestMinPrice)
    println("ClosestMaxPrice: " + closestMaxPrice)
    println("minArea: "+func1(houses))
    println("maxArea: "+func11(houses))
    minArea = func1(houses)
    maxArea = func11(houses)
    closestMinPrice = funcAvgMinPrice(minArea, houses)
    println("closestMinPrice: "+closestMinPrice)
    closestMaxPrice = funcAvgMinPrice(maxArea, houses)
    println("closestMaxPrice: "+closestMaxPrice)

    if(isBuyersHouseGreater) {
      //println("*-*-*-*-*-*-*- Extrapolate")
      //houses.map(x => x.area).toArray.foreach(println)
      //println("---------")
      closestMinVal = getClosestMin(reqArea, houses.map(x => x.area).toArray)
      println("closestMinVal recalculado: "+closestMinVal)
      var meanPriceOfLessSquareFootHouses: Long = 0L
      var acc: Long = 0L
      var n: Long = 0L
      for(i <- 0 until houses.size){
        if(closestMinVal == houses(i).area) {
          acc += houses(i).price
          n += 1
        }
      }
      meanPriceOfLessSquareFootHouses = acc / n

      //                    (25,000                   - 16000                         ) / (2000          - 1200         ) = 11.25
      var division: Float = (closestMaxPrice.toFloat - meanPriceOfLessSquareFootHouses) / (closestMaxVal - closestMinVal)
      //println("division: "+division)

      //       16000           + (2500    - 2000   ) * 11.25
      answer = closestMaxPrice + (reqArea - maxArea) * division
    }else{
      // interpolate
      //                     (30,000                 - 13,000)          / (2000    -    1200) = 21.25
      //
      val division: Float = (closestMaxPrice.toFloat - closestMinPrice) / (maxArea - minArea)
      //println("division: "+division)

      //       13,000          + 21.25    * (1500    - 1200   )
      //       25,000          +
      answer = closestMinPrice + division * (reqArea - minArea)
    }




    /*for (i <- 0 until areaPriceSize) {
      for (k <- 0 until areaPriceSize) {
        if (!(i == k)) {

        }
      }
    }*/



    answer.toLong
  }

  def funcAvgMinPrice(min: Long, houses: ListBuffer[House]): Long = {
    //println("min: "+min)
    var acc = 0L
    var flag = false
    var size = 0L
    for(i <- 0 until houses.size){
      if(houses(i).area == min && flag == false) {
        acc += houses(i).price
        flag = true
        size = houses(i).compList.size
        for(k <- 0 until houses(i).compList.size){
          //println("houses(i).compList(k): "+houses(i).compList(k))
          acc += houses(i).compList(k)
        }
      }
    }
    //println("acc: "+acc)
    acc / (size + 1)
  }

  def func1(houses: ListBuffer[House]): Long = {
    //println("min: "+houses.map(x => x.area).min)
    houses.map(x => x.area).min
  }

  def func11(houses: ListBuffer[House]): Long = {
    houses.map(x => x.area).max
  }

  def median(n: Long, a: ListBuffer[Long]): Long = {
    val sum = a.foldLeft(0L)(_ + _) + n
    val numOfElements = a.size + 1

    sum / numOfElements
  }

  def compListF(areaToCompare: Long, price: Long, areaPrice: Array[(Long, Long)]): (List[Long], Long, Long, Long, Long, Boolean) = {
    //println("compList begin")
    var compList = new ListBuffer[Long]
    for (i <- 0 until areaPrice.size) {
      //println("areaToCompare: ("+ areaToCompare + ", "+areaPrice(i)._1+") | price: ("+price+", "+areaPrice(i)._2+")")
      if (!(areaToCompare, price).equals(areaPrice(i)._1, areaPrice(i)._2)) {
        //println("pasa1")
        if (areaToCompare == areaPrice(i)._1) {
          //println("pasa2")
          compList += areaPrice(i)._2
        }
      }
    }
    //println(compList.size)
    //if(compList.size == 0)
    val compListSize = compList.size
    compList.foreach(println)
    //println("compList end")

    val sum = compList.foldLeft(0L)(_ + _)
    val pm = if (compListSize == 0) {
      1000
    } else {
      sum / compList.size
    }
    var o: Double = 0
    var accumulator = 0L
    for (i <- 0 until compList.size) {
      accumulator += Math.pow((compList(i) - pm), 2).toLong
    }
    if (compListSize > 0) {
      o = Math.sqrt(accumulator / compList.size)
    }

    val priceMinusPm: Long = if(pm > 0){
      if(price - pm != 0) {
        Math.abs(price - pm)
      }else {
        Math.abs(getMaxAbs(price, compList, pm) - pm)
      }
    }else{
      0L
    }

    var isOutliner: Boolean = false

    if (priceMinusPm > (3 * o)) {
      if(compListSize == 0){
        isOutliner = false
      }else{
        isOutliner = true
      }
    }else{
      isOutliner = false
    }

    (compList.toList, pm, o.toLong, priceMinusPm, (3 * o).toLong, isOutliner)
  }



  case class House(val area: Long, val price: Long, val compList: List[Long],
                   val pm: Long = 0, val o: Long = 0, val priceMinusPm: Long = 0, val threeMultO: Long = 0, val isOutlier: Boolean = false)
  object House

  def getMaxAbs(n: Long, a: ListBuffer[Long], pm: Long):Long = {
    val aSize = a.size
    var minValAcc = n
    var minDisAcc = Math.abs(n - pm)
    var flag = false

    for(i <- 0 until aSize) {
      val acc = Math.abs(a(i) - pm)

      if (acc > minDisAcc) {
        minValAcc = a(i)
        minDisAcc = acc
      }
    }
    minValAcc
  }

  def getClosestMax(reqArea: Long, area: Array[Long]): Long = {
    //println("*****************")
    //println("*****************")
    //println("comenzar getClosestMax, recibe: reqArea: "+reqArea+", area: "+area.foreach(println))
    //println("*****************")
    //println("*****************")
    var minValAcc = 0L
    var minDisAcc = 0L
    var flag = false

    var isEveryAreaArrayValueGreaterThanReqArea = false

    // checking if every number is minor than reqArea
    val minAreaArrayNumber = area.min
    val maxAreaArrayNumber = area.max
    if((reqArea > minAreaArrayNumber && reqArea > maxAreaArrayNumber) || (reqArea < minAreaArrayNumber && reqArea < maxAreaArrayNumber)) {
      minValAcc = maxAreaArrayNumber
      isEveryAreaArrayValueGreaterThanReqArea = true
    }

    if(!isEveryAreaArrayValueGreaterThanReqArea) {
      //area.foreach(println)
      println("area: " + area.size)
      println("comenzar getClosestMax")
      for (i <- 0 until area.size) {
        //println("elemento a evaluar: " + area(i))
        if (!isPositive(reqArea - area(i))) {
          //println("elemento negativo: " + (reqArea - area(i)))
          val acc = Math.abs(area(i) - reqArea)

          if (flag == false) {
            //println("minValAcc: " + area(i) + " && minDisAcc: " + acc)
            minValAcc = area(i)
            minDisAcc = acc
            flag = true
          }

          if (acc < minDisAcc) {
            //println("minValAcc: " + area(i) + " && minDisAcc: " + acc)
            minValAcc = area(i)
            minDisAcc = acc

          }
        }
      }
    }

    minValAcc
  }

  def getClosestMin(reqArea: Long, area: Array[Long]): Long = {
    //println("*****************")
    //println("*****************")
    //println("comenzar getClosestMin, recibe: reqArea: "+reqArea+", area: "+area.foreach(println))
    //println("*****************")
    //println("*****************")
    var minValAcc = 0L
    var minDisAcc = 0L
    var flag = false

    var isEveryAreaArrayValueLessThanReqArea = false

    // checking if every number is minor than reqArea
    val minAreaArrayNumber = area.min
    val maxAreaArrayNumber = area.max
    if((reqArea > minAreaArrayNumber && reqArea > maxAreaArrayNumber) || (reqArea < minAreaArrayNumber && reqArea < maxAreaArrayNumber)) {
      minValAcc = minAreaArrayNumber
      isEveryAreaArrayValueLessThanReqArea = true
    }

    if(!isEveryAreaArrayValueLessThanReqArea) {
      //area.foreach(println)
      //println("area: "+area.size)
      for (i <- 0 until area.size) {
        //println("elemento a evaluar: " + area(i))
        if(isPositive(reqArea - area(i))){
          //println("elemento positivo: " + (reqArea - area(i)))
          val acc = Math.abs(area(i) - reqArea)

          if(flag == false) {
            //println("minValAcc: " + area(i) + " && minDisAcc: " + acc)
            minValAcc = area(i)
            minDisAcc = acc
            flag = true
          }

          if (acc < minDisAcc) {
            //println("minValAcc: " + area(i) + " && minDisAcc: " + acc)
            minValAcc = area(i)
            minDisAcc = acc

          }
        }
      }
    }
    //println("*****************")
    //println("*****************")
    //println("*****************")

    minValAcc
  }

  def isPositive(n: Long): Boolean = {
    if(n >= 0){
      true
    }else{
      false
    }
  }

  // example  valuation 19375
  //println(valuation(1500, Array(1200,1300,1200,1300,1200,2000), Array(12000,24000,14000,22000,13000,30000)))

  // sample case 0 valuation 24,000
  //println("valuation: "+valuation(1200, Array(1500,500,1000,2000,2500), Array(30000,10000,20000,40000,50000)))

  // sample case 1   valuation 30625
  //println("valuation: "+valuation(2500, Array(1200,1200,1200,2000), Array(15000,11000,17000,25000)))

  // test case 2 valuation 17996
  //println("valuation: "+valuation(15857,
  //  Array(95849,95849,20995,20995,20995,20995,20995,95849,95849,15857),
  //  Array(922271,860302,222528,560356,68326,493430,508605,565375,849039,17996)))

  // test case 3 valuation 1,000,000
  println("valuation: "+valuation(83315,
    Array(6419,60833,66886,94528,40561,60833,40561,71240,61750,9554,59632,14676,85362,78298,94592,1413,53847,45576,34609,78298,18911,66886,72650,94592,68371,2477,92630,34063,53847,9554,11818,92630,68371,81382,20445,60148,34609,85362,60148,14676,72660,68370,47039,20445,3347,94528,81382,31019,70640,68370),
    Array(49982,401935,506106,873017,256890,462278,244270,630664,479989,79432,365569,131616,781296,715813,979700,17272,312553,275554,244055,716031,136254,552175,666694,952588,587266,30375,802517,231884,325420,57478,101678,807987,618748,741461,140773,375329,235543,758976,384469,135101,668074,585777,281995,141638,43866,889909,738941,167466,628749,564846)))
}
