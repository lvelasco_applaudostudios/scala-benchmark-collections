package com.scala.benchmark.examples

object ForExpressions extends App {
  val xValues = 1 to 4
  val yValues = 1 to 2
  val coordinates = for {
    x <- xValues
    y <- yValues
  } yield (x, y)
  println("xValues: " + xValues)
  coordinates.foreach(println)
  println("posicion 4: "+coordinates(4))

  val nums = List(List(1), List(2), List(3), List(4), List(5))


  val result = for {
    numList <- nums
    num <- numList
    if (num % 2 == 0)
  } yield (num)
  println(result)

  println(nums.flatMap(numList => numList))

  println(nums.flatten.filter(_ % 2 == 0))
  println(nums.flatMap(numList => numList).filter(_ % 2 == 0))


  val g: String = "Check out the big brains on Brad!"
  println(g indexOf 'o')
  println(g.indexOf('o',7))
}
