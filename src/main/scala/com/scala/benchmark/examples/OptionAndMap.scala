package com.scala.benchmark.examples

object OptionAndMap extends App {
  val number: Option[Int] = Some(3)
  val noNumber: Option[Int] = None
  val result1 = number.map(_ * 1.5)
  val result2 = noNumber.map(_ * 1.5)
  val number1: Option[Int] = Some(3)
  val noNumber1: Option[Int] = None
  val result11 = number1.fold(1)(_ * 3)
  val result21 = noNumber1.fold(1)(_ * 3)
  println(result1)
  println(result2)

  println(result11)
  println(result21)
}
