package com.scala.benchmark.examples

object PatternMatching extends App {
  val stuff = "blue"

  val myStuff = stuff match {
    case "red" =>
      println("RED"); 1
    case "blue" =>
      println("BLUE"); 2
    case "green" =>
      println("GREEN"); 3
    case _ =>
      println(stuff); 0 // case _ will trigger if all other cases fail.
  }

  val myStuff1 = stuff match {
    case "red" => (255, 0, 0)
    case "green" => (0, 255, 0)
    case "blue" => (0, 0, 255)
    case _ => println(stuff); 0
  }

  def goldilocks(expr: Any) =
    expr match {
      case ("porridge", "Papa") => "Papa eating porridge"
      case ("porridge", "Mama") => "Mama eating porridge"
      case ("porridge", "Baby") => "Baby eating porridge"
      case _ => "what?"
    }

  def goldilocks1(expr: Any) =
    expr match {
      case ("porridge", _) => "eating"
      case ("chair", "Mama") => "sitting"
      case ("bed", "Baby") => "sleeping"
      case _ => "what?"
    }

  def goldilocks2(expr: (String, String)) =
    expr match {
      case ("porridge", bear) => bear + " said someone's been eating my porridge"
      case ("chair", bear) => bear + " said someone's been sitting in my chair"
      case ("bed", bear) => bear + " said someone's been sleeping in my bed"
      case _ => "what?"
    }
  println(goldilocks2(("porridge", "Papa")))


  val foodItem = "porridge"

  def goldilocks3(expr: (String, String)) =
    expr match {
      case (`foodItem`, _) => "eating"
      case ("chair", "Mama") => "sitting"
      case ("bed", "Baby") => "sleeping"
      case _ => "what?"
    }

  goldilocks3(("porridge", "Papa"))

  def patternEquals(i: Int, j: Int) =
    j match {
      case `i` => true
      case _ => false
    }
  println(patternEquals(3, 3))

  val secondElement = List(1, 2, 3) match {
    case x :: xs => xs.head
    case _ => 0
  }

  println(secondElement)


  val secElement = List(1, 2, 3) match {
    case x :: y :: xs => y
    case _ => 0
  }
  println("secElement: "+secElement)


  val r = List(1, 2, 3) match {
    case x :: y :: Nil => y // only matches a list with exactly two items
    case _ => 0
  }
  println(r)


  val r1 = List(1, 2, 3) match {
    case x :: y :: z :: tail => tail
    case _ => 0
  }
  println(r1)


}
