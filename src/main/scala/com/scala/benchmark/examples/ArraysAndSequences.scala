package com.scala.benchmark.examples

object ArraysAndSequences extends App {
  // Scala provides a data structure, the array, which stores a fixed-size sequential collection of elements of the same type. An array is used to store a collection of data, but it is often more useful to think of an array as a collection of variables of the same type.
  //
  //A list can be converted to an array:
  val l = List(1, 2, 3)
  val a = l.toArray

  val s = Seq("hello", "to", "you")
  val filtered = s.filter(_.length > 2)
  println(filtered)

  val a1 = Array("hello", "to", "you", "again")
  val filtered1 = a1.filter(_.length > 3)
  println(filtered1)

  val s1 = Seq("hello", "world")
  val r = s1 map {
    _.reverse
  }
  println(r)

}
