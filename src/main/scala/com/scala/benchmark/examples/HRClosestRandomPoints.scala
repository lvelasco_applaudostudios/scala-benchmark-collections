package com.scala.benchmark.examples

object HRClosestRandomPoints extends App {


  def closestSquaredDistance(x: Array[Int], y: Array[Int]): Long = {
    //val res = 1L
    //println("x: " + x)
    //println("y: " + y)
    //x.foreach(println)
    //y.foreach(println)
    val tuples1 = x.zip(y)
    val tuples2 = tuples1
    var minValAcc: Double = 0
    var firstTimeFlag = false

    val tupleSize = tuples1.size

    if(tupleSize >= 2 && (tupleSize <= 1000 | tupleSize == Math.pow(10,5))) {
      for (i <- 0 until tupleSize) {
        for (k <- 0 until tupleSize) {
          println(tuples1(i)._1 + " - " + tuples2(k)._1 + " && " + tuples1(i)._2 + " - " + tuples2(k)._2)

          //if(!tuples1(i).equals(tuples2(k))) {
          if (!(i == k)) {
            println(tuples1(i)._1 + " - " + tuples2(k)._1 + " && " + tuples1(i)._2 + " - " + tuples2(k)._2)
            val acc: Double = Math.pow(tuples1(i)._1 - tuples2(k)._1, 2) + Math.pow(tuples1(i)._2 - tuples2(k)._2, 2)

            if (firstTimeFlag == false) {
              minValAcc = acc
              firstTimeFlag = true
            }
            if (acc < minValAcc) {
              println("minValAcc: " + minValAcc + " vs acc: " + acc)
              minValAcc = acc
            }
          }
        }
      }
    }

    minValAcc.toLong
  }

  //println(closestSquaredDistance(Array(77, 1000, 992, 1000000), Array(0, 1000, 500, 0)))
  println(closestSquaredDistance(Array(542243, 5000), Array(0, 322)))
}
