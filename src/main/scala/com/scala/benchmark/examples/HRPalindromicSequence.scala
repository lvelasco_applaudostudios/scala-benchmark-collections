package com.scala.benchmark.examples

import scala.collection.mutable.ListBuffer

object HRPalindromicSequence extends App {

  def maxScore(s: String): Int = {
    var maxScore = 0
    val str: ListBuffer[String] = subPal(s)

    var firstIteration = true
    var a = ""
    var aSize = 0

    var b = ""
    var bSize = 0

    for(i <- 0 until str.size) {
      println("iteration "+i+": "+str(i))

      if(i == 0 && firstIteration == true){
        a = str(i)
        aSize = str(i).size
        firstIteration = false
      }
      if(str(i).length > a.size && firstIteration == false){
        a = str(i)
        aSize = str(i).size
      }
      if(a != str(i) && b != a && str(i).size > bSize) {
        b = str(i)
        bSize = str(i).size
      }
    }
    println("aSize: "+aSize)
    println("bSize: "+bSize)
    maxScore = aSize * bSize

    maxScore
  }

  def subPal(str: String): ListBuffer[String] = {
    var s1 = ""
    var n: Int = str.size
    var count = 0
    var palindromeArray = new ListBuffer[String]
    println("Given string: " + str)
    val un = 1 until n+1
    println("1 until n+1: "+un)
    for(i <- un){
      println(i)
      for(j <- 0 until n+1){
        var k = i + j - 1
        if(! (k >= n)) {
          s1 = str.substring(j, i + j)
          if(s1.equals(new StringBuilder(s1).reverse.toString)){
            palindromeArray.addOne(s1)
          }
        }

      }
    }

    for(i <- 0 until palindromeArray.size) {
      println(palindromeArray(i) + " is a palindrome string")
    }
    println("The no. of substrings that are palindrome: " + palindromeArray.size)

    palindromeArray
  }

  //println("attract".length)
  //println("attract".size)
  //maxScore("attract")
  println(maxScore("abcacbbbca"))
  println(maxScore("acdapmpomp"))
  println(maxScore("axbawbaseksqke"))
  //maxScore("google")
}
