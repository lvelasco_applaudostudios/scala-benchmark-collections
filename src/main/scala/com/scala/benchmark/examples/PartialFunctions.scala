package com.scala.benchmark.examples

object PartialFunctions extends App {

  val doubleEvens: PartialFunction[Int, Int] =
    new PartialFunction[Int, Int] {
      //States that this partial function will take on the task
      def isDefinedAt(x: Int) = x % 2 == 0

      //What we do if this partial function matches
      def apply(v1: Int) = v1 * 2
    }

  val tripleOdds: PartialFunction[Int, Int] = new PartialFunction[Int, Int] {
    def isDefinedAt(x: Int) = x % 2 != 0

    def apply(v1: Int) = v1 * 3
  }

  val whatToDo = doubleEvens orElse tripleOdds

  println(whatToDo(3)) //9
  println(whatToDo(4)) //8

  //Case statements are a quick way to create partial functions. When you create a case statement, the apply and isDefinedAt methods are created automatically
  val doubleEvens1: PartialFunction[Int, Int] = {
    case x if (x % 2) == 0 => x * 2
  }
  val tripleOdds1: PartialFunction[Int, Int] = {
    case x if (x % 2) != 0 => x * 3
  }

  val whatToDo1 = doubleEvens1 orElse tripleOdds1 //Here we chain the partial functions together
  println(whatToDo1(3))
  println(whatToDo1(4))

  // The result of partial functions can have an andThen function added to the end of the chain:
  val doubleEvens2: PartialFunction[Int, Int] = {
    case x if (x % 2) == 0 => x * 2
  }
  val tripleOdds2: PartialFunction[Int, Int] = {
    case x if (x % 2) != 0 => x * 3
  }

  val addFive = (x: Int) => x + 5
  val whatToDo2 =
    doubleEvens2 orElse tripleOdds2 andThen addFive //Here we chain the partial functions together
  println(whatToDo2(3))
  println(whatToDo2(4))

  val doubleEvens4: PartialFunction[Int, Int] = {
    case x if (x % 2) == 0 => x * 2
  }
  val tripleOdds4: PartialFunction[Int, Int] = {
    case x if (x % 2) != 0 => x * 3
  }

  val printEven: PartialFunction[Int, String] = {
    case x if (x % 2) == 0 => "Even"
  }
  val printOdd: PartialFunction[Int, String] = {
    case x if (x % 2) != 0 => "Odd"
  }

  val whatToDo4 = doubleEvens4 orElse tripleOdds4 andThen (printEven orElse printOdd)

  println(whatToDo4(3)) // Odd
  println(whatToDo4(4)) // Even
}
