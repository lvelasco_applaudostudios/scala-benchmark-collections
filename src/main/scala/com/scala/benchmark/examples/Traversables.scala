package com.scala.benchmark.examples

import scala.collection.immutable.LazyList.cons
import scala.language.postfixOps

object Traversables extends App {

  val set = Set(1, 9, 10, 22)
  val list = List(3, 4, 5, 10)
  val result = set ++ list
  println(result.size)

  val result2 = list ++ set
  println(result2.size)

  val set1 = Set(1, 3, 4, 6)
  val result1 = set1.map(_ * 4)
  println(result1.lastOption)
  println(result1.last)

  //flatten will "pack" all child Traversables into a single Traversable
  val list1 = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
  println(list1.flatten)

  val list4 = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
  val result4 = list4.flatMap(_.map(_ * 4))
  println(result4)


  //flatMap of Options will filter out all None S but keep the Some S
  val list2 = List(1, 2, 3, 4, 5)
  val result3 = list2.flatMap(it => if (it % 2 == 0) Some(it) else None)
  //val result4 = list2.flatMap(it => if (it % 2 == 0) it else None)
  println(result3)


  val list5 = List(4, 6, 7, 8, 9, 13, 14)
  val result5 = list5.collect {
    case x: Int if (x % 2 == 0) => x * 3
  }
  println(result5)


  //val list6 = List(4, 6, 7, 8, 9, 13, 14)
  val list6 = List(7, 9, 13)
  val partialFunction1: PartialFunction[Int, Int] = {
    case x: Int if x % 2 == 0 => x * 3
  }
  val partialFunction2: PartialFunction[Int, Int] = {
    case y: Int if y % 2 != 0 => y * 4
  }
  val result6 = list6.collect(partialFunction1 orElse partialFunction2)
  println(result6)

  // foreach retorna Unit
  val list7 = List(4, 6, 7, 8, 9, 13, 14)
  list7.foreach(num => println(num * 4))
  println(list7)


  val list8 = List(4, 6, 7, 8, 9, 13, 14)
  val result8 = list8.to(LazyList)
  println(result.isInstanceOf[LazyList[_]])
  println((result8 take 3)) // LazyList(4, 6, 7)

  val mapp = Map("Phoenix" -> "Arizona", "Austin" -> "Texas")
  println(mapp.knownSize)

  val map1 = Map("Phoenix" -> "Arizona", "Austin" -> "Texas")
  println(map1.knownSize) //2

  val stream1 = cons(0, cons(1, LazyList.empty))
  println(stream1.knownSize) // -1

  val list9 = List(10, 19, 45, 1, 22)
  println(list9.find(_ % 2 != 0)) //Some(19)

  val list10 = List(4, 8, 16)
  println(list10.find(_ % 2 != 0)) //None

  val list11 = List(10, 19, 45, 1, 22)
  println(list11.init) // List(10, 19, 45, 1)

  val list12 = List(10, 19, 45, 1, 22)
  println(list12.slice(1, 3))


  // take is used often with LazyList, since they are also Traversable
  def makeLazyList(v: Int): LazyList[Int] = cons(v, makeLazyList(v + 1))
  val a = makeLazyList(2)
  println((a take 3 toList))

  //
  def makeLazyList1(v: Int): LazyList[Int] = cons(v, makeLazyList(v + 1))
  val a11 = makeLazyList1(2)
  println(((a11 drop 6) take 3).toList)

  // takeWhile will continually accumulate elements until a predicate is no longer satisfied:
  val list123 = List(87, 44, 5, 4, 200, 10, 39, 100)
  println(list123.takeWhile(_ < 100))

  // dropWhile will continually drop elements until a predicate is no longer satisfied:
  val list321 = List(87, 44, 5, 4, 200, 10, 39, 100)
  println(list321.dropWhile(_ < 100))

  val arraya = Array(87, 44, 5, 4, 200, 10, 39, 100)
  println(arraya.filterNot(_ < 100).toList) //Array(200, 100)

  // splitAt will split a Traversable at a position, returning a 2 product Tuple. splitAt is also defined as (xs take n, xs drop n)
  val array11 = Array(87, 44, 5, 4, 200, 10, 39, 100)
  val result11 = array11 splitAt 3
  println(result11)
  println("splitAt: "+result11._1.toList)
  println("splitAt: "+result11._2.toList)


  // span will split a Traversable according to a predicate, returning a 2 product Tuple. span is also defined as (xs takeWhile p, xs dropWhile p)
  val arrayaa = Array(87, 44, 5, 4, 200, 10, 39, 100)
  val resultaa = arrayaa span (_ < 100)
  println(resultaa._1.toList) // Array(87, 44, 5, 4)
  println(resultaa._2.toList) // Array(200, 10, 39, 100)

  // partition will split a Traversable according to a predicate, returning a 2 product Tuple. The left-hand side contains the elements satisfied by the predicate whereas the right hand side contains the rest of the elements. partition is also defined as (xs filter p, xs filterNot p)
  val arraybb = Array(87, 44, 5, 4, 200, 10, 39, 100)
  val resultbb = arraybb partition (_ < 100)
  println(resultbb._1.toList) //Array(87,44,5,4,10,39)
  println(resultbb._2.toList) //Array(200,100)


  //forall will determine if a predicate is valid for all members of a Traversable
  val listasd = List(87, 44, 5, 4, 200, 10, 39, 100)
  val resultasd = listasd forall (_ < 100)
  println(resultasd)

  //exists will determine if a predicate is valid for some members of a Traversable:
  val listdsa = List(87, 44, 5, 4, 200, 10, 39, 100)
  val resultdsa = listdsa exists (_ < 100)
  println(resultdsa)

  //count will count the number of elements that satisfy a predicate in a Traversable
  val listqwe = List(87, 44, 5, 4, 200, 10, 39, 100)
  val resultqwe = listqwe count (_ < 100)
  println(resultqwe)

  //foldLeft will combine an operation starting with a seed and combining from the left.
  // foldLeft takes as a first parameter the initial value of the fold. Once the fold is
  // established, you provide a function that takes two arguments.
  // The first argument is the running total of the operation, and the second element is
  // the next element of the list.
  //
  //Given a Traversable (x1, x2, x3, x4), an initial value of init, an operation op,
  // foldLeft is defined as: (((init op x1) op x2) op x3) op x4)
  val listrty = List(5, 4, 3, 2, 1)
  val resultrty = listrty.foldLeft(0) { (`running total`, `next element`) =>
    `running total` - `next element`
  }
  println(result) // -15

  val result22 = list.foldLeft(0)(_ - _) //Short hand
  println(result22) // -15

  println((((((0 - 5) - 4) - 3) - 2) - 1)) // -15


  // foldRight will combine an operation starting with a seed and combining from the right.
  // foldRight takes as a first parameter the initial value of the fold. Once the fold is
  // established, you provide a function that takes two elements. The first is the next element
  // of the list, and the second element is the running total of the operation.
  //
  //Given a Traversable (x1, x2, x3, x4), an initial value of init, an operation op, foldRight is
  // defined as: x1 op (x2 op (x3 op (x4 op init)))
  val listdfg = List(5, 4, 3, 2, 1)
  val resultdfg = listdfg.foldRight(0) { (`next element`, `running total`) =>
    `next element` - `running total`
  }
  println(resultdfg) // 3

  val result2a = list.foldRight(0)(_ - _) //Short hand      3
  println(result2a)

  println((5 - (4 - (3 - (2 - (1 - 0))))))  // 3

  // reduceLeft is similar to foldLeft, except that the seed is the head value
  val intList = List(5, 4, 3, 2, 1)
  println(intList.reduceLeft {
    _ + _
  })

  val stringList = List("Do", "Re", "Me", "Fa", "So", "La", "Te", "Do")
  println(stringList.reduceLeft {
    _ + _
  })

  //reduceRight is similar to foldRight, except that the seed is the last value
  val intLista = List(5, 4, 3, 2, 1)
  println(intLista.reduceRight {
    _ + _
  })

  val stringLista = List("Do", "Re", "Me", "Fa", "So", "La", "Te", "Do")
  println(stringLista.reduceRight {
    _ + _
  })

  //There are some methods that take much of the folding work out by providing basic functionality.
  // sum will add all the elements, product will multiply, min would determine the smallest
  // element, and max the largest:
  val intListv = List(5, 4, 3, 2, 1)
  println(intListv.sum)
  println(intListv.product)
  println(intListv.max)
  println(intListv.min)

  //The naive recursive implementation of reduceRight is not tail recursive and would lead to a stack overflow
  // if used on larger traversables. However, reduceLeft can be implemented with tail recursion.
  //
  //To avoid the potential stack overflow with the naive implementation of reduceRight we can easily
  // implement it based on reduceLeft by reversing the list and the inverting the reduce function.
  // The same applies for folding operations.
  //
  //There is also a reduce (and fold) available, which works exactly like reduceLeft (and foldLeft)
  // and it should be the prefered method to call unless there is a strong reason to use reduceRight (or foldRight)

  val intListn = List(5,4,3)
  println("reduce: "+intListn.reduce((x,y) => y - x))
  // ((((4 - 5) = -1
  // 3 - (-1) = 4
  // 2 - 4 = -2
  // 1 - (-2) = 3


  println(intListn.reduceRight((x, y) => x - y))
  println(intListn.reverse.reduceLeft((x, y) => y - x))
  println(intListn.reverse.reduce((x, y) => y - x))

  //transpose will take a traversable of traversables and group them by their position in it's own traversable,
  // e.g.: ((x1, x2),(y1, y2)).transpose = (x1, y1), (x2, y2) or ((x1, x2, x3),(y1, y2, y3),(z1, z2, z3)).
  // transpose = ((x1, y1, z1), (x2, y2, z2), (x3, y3, z3))
  val listhj = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
  println(listhj.transpose) // List(List(1, 4, 7), List(2, 5, 8), List(3, 6, 9))

  val list2a = List(List(1), List(4))
  println(list2a.transpose) // List(List(1, 4))

  //mkString will format a Traversable using a given string as the delimiter:
  val listjk = List(1, 2, 3, 4, 5)
  println(listjk.mkString(",")) //1,2,3,4,5
  println(listjk.mkString(".")) //1.2.3.4.5

  //mkString will also take a beginning and ending string to surround the list.:
  val listop = List(1, 2, 3, 4, 5)
  println(listop.mkString(">", ",", "<")) //>1,2,3,4,5<

  //addString will take a StringBuilder to add the contents of list into the builder.
  val stringBuilder = new StringBuilder()
  val listl = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
  stringBuilder.append("I want all numbers 6-12: ")
  println("filter: "+listl.filter(it => it > 5 && it < 13))
  //println(listl.filter(it => it > 5 && it < 13).addString(stringBuilder, ","))
  listl.filter(it => it > 5 && it < 13).addString(stringBuilder, ",")
  //println(stringBuilder)
  println(stringBuilder.mkString)
}
