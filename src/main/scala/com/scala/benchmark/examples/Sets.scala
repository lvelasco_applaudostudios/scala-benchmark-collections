package com.scala.benchmark.examples

object Sets extends App {
  val mySet = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
  val mySetDistinct = Set("Michigan", "Ohio", "Wisconsin", "Michigan")
  val aNewSet = mySet + "Illinois"
  val mySet1 = Set("Michigan", "Ohio", 12)
  println(mySet1(12))
  val aNewSet1 = mySet - "Michigan"
  val aNewSet11 = mySet -- List("Michigan", "Ohio")

  val mySet11 = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
  val mySet22 = Set("Wisconsin", "Michigan", "Minnesota")
  val aNewSet1212 = mySet11 intersect mySet22 // NOTE: You can use the "&" operator
  println(aNewSet1212)

  val mySet111 = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
  val mySet222 = Set("Wisconsin", "Michigan", "Minnesota")
  val aNewSet1122 = mySet111 union mySet222 // NOTE: You can also use the "|" operator
  println(aNewSet1122)

  val mySetA = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
  val mySetB = Set("Wisconsin", "Michigan", "Minnesota")
  val mySetC = Set("Wisconsin", "Michigan")

  println(mySetB subsetOf mySetA)
  println(mySetC subsetOf mySetA)

  val mySetAA = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
  val mySetBB = Set("Wisconsin", "Michigan")
  val aNewSetAB = mySetAA diff mySetBB // Note: you can use the "&~" operator if you *really* want to.

  println(aNewSetAB.equals(Set("Ohio", "Iowa")))
}
