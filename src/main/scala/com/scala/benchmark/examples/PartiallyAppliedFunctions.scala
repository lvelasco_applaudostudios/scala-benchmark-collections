package com.scala.benchmark.examples

object PartiallyAppliedFunctions extends App {
  def sum(a: Int, b: Int, c: Int) = a + b + c
  val sum3 = sum _
  println(sum3(1, 9, 7))
  println(sum(4, 5, 6))

  def sum1(a: Int, b: Int, c: Int) = a + b + c
  val sumC = sum1(1, 10, _: Int)
  println(sumC(4))
  println(sum1(4, 5, 6))

  //Currying is a technique to transform a function with multiple parameters into multiple functions which each take one parameter:
  def multiply(x: Int, y: Int) = x * y

  println((multiply _).isInstanceOf[Function2[_, _, _]])
  val multiplyCurried = (multiply _).curried
  println(multiply(4, 5))
  println(multiplyCurried(3)(2))
  val multiplyCurriedFour = multiplyCurried(4)
  println(multiplyCurriedFour(2))
  println(multiplyCurriedFour(4)) //16


  def customFilter(f: Int => Boolean)(xs: List[Int]) =
    xs filter f
  def onlyEven(x: Int) = x % 2 == 0
  val xs = List(12, 11, 5, 20, 3, 13, 2)
  println(customFilter(onlyEven)(xs))

  val onlyEvenFilter = customFilter(onlyEven) _
  println(onlyEvenFilter(xs))
}
