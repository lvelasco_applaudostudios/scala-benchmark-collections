package com.scala.benchmark.examples

object Lists extends App {
  val a = List(1, 3, 5, 7, 9)
  println(a(4))

  println(a.filterNot(v => v == 5))

  a.length
  a.reverse
  a.map { v =>
    v * 2
  }

  a.filter { v =>
    v % 3 == 0
  }

  val a1 = List(1, 2, 3)

  a1.map {
    _ * 2
  }

  a1.filter {
    _ % 2 == 0
  }

  val reduceLeft = List(1,3,5,7)
  println(reduceLeft.reduceLeft(_ + _))
  println(reduceLeft.reduceLeft(_ * _))
  println(reduceLeft.foldLeft(0)(_ + _))

  val d = Nil
  val c = 3 :: d
  val b = 2 :: c
  val a2 = 1 :: b

  println(a2.tail)
}
