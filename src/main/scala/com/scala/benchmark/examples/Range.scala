package com.scala.benchmark.examples

object Range extends App {
  val someNumbers = 0 until 10 by 1
  val second = someNumbers(1)
  val last = someNumbers.last
  println(someNumbers)
  println(someNumbers.size)
  println(second)
  println(last)



  //A range does not include its upper bound, even in a step increment:

  val someNumbers1 = 0 until 34 by 2 // Range(0, 34, 2)
  someNumbers1.contains(33)
  someNumbers1.contains(32)
  someNumbers1.contains(34)
}
