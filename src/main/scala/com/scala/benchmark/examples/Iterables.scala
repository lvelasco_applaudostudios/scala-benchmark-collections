package com.scala.benchmark.examples

object Iterables extends App {
  /*
  The next trait from the top in the collections hierarchy is Iterable. All methods in this trait are defined in terms of an abstract method, iterator, which yields the collection's elements one by one. The foreach method from trait Traversable is implemented in Iterable in terms of iterator. Here is the actual implementation:

def foreach[U](f: Elem => U): Unit = {
  val it = iterator
  while (it.hasNext) f(it.next())
}
Quite a few subclasses of Iterable override this standard implementation of foreach in Iterable, because they can provide a more efficient implementation. Remember that foreach is the basis of the implementation of all operations in Traversable, so its performance matters.

Some common iterables are Set, List, Vector, Stack and Stream. Iterator has two important methods: hasNext, which answers whether the iterator has another element available, and next which returns the next element in the iterator.
   */
  val list = List(3, 5, 9, 11, 15, 19, 21)
  val it = list.iterator
  if (it.hasNext)
    println(it.next)

  val list1 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
  val it1 = list1 grouped 3
  println(it1.next())

  val list2 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
  val it2 = list2 sliding 3
  println(it2.next())
  println(it2.next())

  //sliding can take the size of the window as well the size of the step during each iteration:
  val list3 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
  val it3 = list3 sliding (3, 3)
  println(it3.next())
  println(it3.next())
  println(it3.next())

  val list4 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
  println((list4 takeRight 3))

  val list5 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
  println((list5 dropRight 3))

  val xs = List(3, 5, 9)
  val ys = List("Bob", "Ann", "Stella")
  println((xs zip ys))


  val xs1 = List(3, 5, 9)
  val ys1 = List("Bob", "Ann")
  println((xs1 zip ys1))

  val xs2 = List(3, 5, 9)
  val ys2 = List("Bob", "Ann")
  println((xs2 zipAll(ys2, -1, "?")))

  val xs3 = List("Manny", "Moe", "Jack")
  println(xs3.zipWithIndex)

  val xs4 = List("Manny", "Moe", "Jack")
  val ys4 = List("Manny", "Moe", "Jack")
  println(xs4.iterator.sameElements(ys4))

  val xt = List("Manny", "Moe", "Jack")
  val yt = List("Manny", "Jack", "Moe")
  println(xt.iterator.sameElements(yt))

  val xs11 = Set(3, 2, 1, 4, 5, 6, 7)
  val ys11 = Set(7, 2, 1, 4, 5, 6, 3)
  println(xs11.iterator.sameElements(ys11))

  val xt1 = Set(1, 2, 3)
  val yt1 = Set(3, 2, 1)
  println(xt1.iterator.sameElements(yt1))
}
