package com.scala.benchmark.examples

object Temporizador {
  def unaVezPorSegundo(repite : () => Unit) {
    while (true) { repite(); Thread sleep 1000 }
  }

  def elTiempoVuela() {
    println("el tiempo pasa volando...")
  }

  def main(args: Array[String]) {
    unaVezPorSegundo(elTiempoVuela)
  }
}
