package com.scala.benchmark.examples

object InfixAndPostfixOperators extends App {
  /*
  Any method which does not require a parameter can be used as a postfix operator: a.m can be written as a m.

  For instance, a.+(b) is equivalent to a + b and a.! is the same as a!.

  Postfix operators have lower precedence than infix operators, so:

  foo bar baz means foo.bar(baz).
  foo bar baz bam means (foo.bar(baz)).bam
  foo bar baz bam bim means (foo.bar(baz)).bam(bim)
   */
  val g: Int = 31
  println(g.toHexString)

  val g1: Int = 31
  println((-g1))

  class Stereo {
    def unary_+ = "on"

    def unary_- = "off"
  }

  val stereo = new Stereo
  println(+stereo)
  println(-stereo)

}
