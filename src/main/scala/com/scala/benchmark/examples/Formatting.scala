package com.scala.benchmark.examples

object Formatting extends App {
  val s = "Hello World"
  println("Application %s".format(s))

  val c = 'a' //unicode for a
  val e = '\"'
  val f = '\\'

  println("%c".format(c))
  println("%c".format(e))
  println("%c".format(f))

  val j = 190
  println("%d bottles of beer on the wall" format j - 100)

  val i = 190
  val k = "vodka"

  "%d bottles of %s on the wall".format(i - 100, k)
}
