package com.scala.benchmark.examples

object FizzBuzz extends App {

  fizzBuzz(15)

  def fizzBuzz(n: Int) {

    for(i <- 1 to n) {

      var three = false
      var five = false


      if (i % 3 == 0)
        three = true

      if (i % 5 == 0)
        five = true

      if (three == true && five == true)
        println("FizzBuzz")

      if (three == true && five == false)
        println("Fizz")

      if (three == false && five == true)
        println("Buzz")

      if (three == false && five == false)
        println(i)

    }
  }
}
