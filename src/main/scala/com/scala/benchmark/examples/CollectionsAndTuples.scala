package com.scala.benchmark.examples

object CollectionsAndTuples extends App {
  val lista = List(("apple", "dog"), ("banana", "duck"), ("strawberry", "frog"), ("mango", "cat"))
  val lista1 = List(("apple", "dog"), ("banana", "duck"), ("strawberry", "frog"), ("mango", "cat"))
  println("iguales? " + lista.equals(lista1))
  //println(lista)
  lista.foreach({ x =>
    println(x._1 + " - " + x._2)
  })

  for(i <- 0 until lista.size){
    println(lista(i)._1 + " | " + lista(i)._2)
  }


}
