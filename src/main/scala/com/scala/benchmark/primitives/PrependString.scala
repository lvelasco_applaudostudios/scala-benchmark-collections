package com.scala.benchmark.primitives
import com.scala.benchmark.utils.ExecutionTime.operationMeasurement
import scala.io.Source

object PrependString extends App {
  var file1TenThousand = Source.fromFile("src/main/resources/Text1.txt").bufferedReader.readLine
  var file2TenThousand = Source.fromFile("src/main/resources/Text2.txt").bufferedReader.readLine
  var file1FiveMillion = (file1TenThousand + " ") * 500
  var file2FiveMillion = (file2TenThousand + " ") * 500
  //var test1 = Source.fromFile("src/main/resources/Text1c.txt").bufferedReader.readLine
  //var test2 = Source.fromFile("src/main/resources/Text2c.txt").bufferedReader.readLine
  //println(file1)
  //println(file2)
  //test1 *= 2500
  //test2 *= 500
  //println(file1.split(" ").toList)
  //println(file1FiveMillion.split(" ").toList.size)
  //println(file2.split(" ").toList)
  //println(file2.split(" ").toList.size)
  //println(test1.split(" ").toList.prependedAll(test2.split(" ").toList))
  //println(test1.split(" ").toList.size)


  val tenThousand = 10000
  val fiveMillion = 5000000

  //file1.toString.split(" ")
  //test1 *= 5
  //test1 *= 5
  //println(test1.toList)
  //println(test2.toList)

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var listA = file1TenThousand.split(" ").toList
  var listB = file2TenThousand.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  var durationTimeInMs = 0L
  durationTimeInMs = operationMeasurement {
    //listA.prependedAll(listB)
    listA ++ listB
  }
  println(s"""Operation [String]: listA ++ listB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  // one million
  listA = file1FiveMillion.split(" ").toList
  listB = file2FiveMillion.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  durationTimeInMs = operationMeasurement {
    //listA.prependedAll(listB)
    listA ++ listB
  }
  println(s"""Operation [String]: listA ++ listB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var vectorA = file1TenThousand.split(" ").toVector
  var vectorB = file2TenThousand.split(" ").toVector

  durationTimeInMs = operationMeasurement {
    //vectorA.prependedAll(vectorB)
    vectorA ++ vectorB
  }
  println(s"""Operation [String]: vectorA ++ vectorB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  vectorA = file1FiveMillion.split(" ").toVector
  vectorB = file2FiveMillion.split(" ").toVector
  durationTimeInMs = operationMeasurement {
    //vectorA.prependedAll(vectorB)
    vectorA ++ vectorB
  }
  println(s"""Operation [String]: vectorA ++ vectorB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */
  //tenThousand
  var seqA = file1TenThousand.split(" ").toSeq
  var seqB = file2TenThousand.split(" ").toSeq

  durationTimeInMs = operationMeasurement {
    //seqA.prependedAll(seqB)
    seqA ++ seqB
  }
  println(s"""Operation [String]: seqA ++ seqB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  seqA = file1FiveMillion.split(" ").toSeq
  seqB = file2FiveMillion.split(" ").toSeq
  durationTimeInMs = operationMeasurement {
    //seqA.prependedAll(seqB)
    seqA ++ seqB
  }
  println(s"""Operation [String]: seqA ++ seqB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */
  //ten thousand
  var arrayA = file1TenThousand.split(" ").toArray
  var arrayB = file2TenThousand.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    //arrayA.prependedAll(arrayB)
    arrayA ++ arrayB
  }
  println(s"""Operation [String]: arrayA ++ arrayB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  // one million
  arrayA = file1FiveMillion.split(" ").toArray
  arrayB = file2FiveMillion.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    //arrayA.prependedAll(arrayB)
    arrayA ++ arrayB
  }
  println(s"""Operation [String]: arrayA ++ arrayB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")
}
