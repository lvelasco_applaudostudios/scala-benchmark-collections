package com.scala.benchmark.primitives
import com.scala.benchmark.utils.ExecutionTime.operationMeasurement
object Transform extends App {
  val testRange1 = 1 to 10
  val testRange2 = 11 to 20
  val test = 1 to 20

  //tenThousand
  val tenThousandRange1 = 1 to 10000
  val tenThousandRange2 = 10001 to 20000
  val tenThousand = 10000

  // oneMillion
  val oneMillionRange1 = 1 to 500000
  val oneMillionRange2 = 500001 to 1000000
  val oneMillion = 1000000

  // fiveMillion
  val fiveMillionRange1 = 1 to 5000000
  val fiveMillionRange2 = 5000001 to 10000000
  val fiveMillion = 5000000

  //println(testRange1.toList)
  //println(testRange2.toList)

  //var listA = testRange1.toList
  //var listB = testRange2.toList
  //println(listA ++ listB)
  //var vectorA = testRange1.toVector
  //var vectorB = testRange2.toVector
  //println(vectorA ++ vectorB)
  //var seqA = testRange1.toSeq
  //var seqB = testRange2.toSeq
  //println(seqA ++ seqB)
  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA = fiveMillionRange1.toList
  var durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    listA.toVector
  }
  println(s"Operation [Int]: listA toVector for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    listA.toSeq
  }
  println(s"Operation [Int]: listA toSeq for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    listA.toArray
  }
  println(s"Operation [Int]: listA toArray for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA = fiveMillionRange1.toVector
  durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    vectorA.toList
  }
  println(s"Operation [Int]: vectorA toList for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    vectorA.toSeq
  }
  println(s"Operation [Int]: vectorA toSeq for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    listA.toArray
  }
  println(s"Operation [Int]: vectorA toArray for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA = fiveMillionRange1.toSeq
  durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    seqA.toList
  }
  println(s"Operation [Int]: seqA toList for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    seqA.toVector
  }
  println(s"Operation [Int]: seqA toVector for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    seqA.toArray
  }
  println(s"Operation [Int]: seqA toArray for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA = fiveMillionRange1.toArray
  durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    arrayA.toList
  }
  println(s"Operation [Int]: arrayA toList for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    arrayA.toVector
  }
  println(s"Operation [Int]: arrayA toVector for $fiveMillion processed in: " + durationTimeInMs + " ms")

  durationTimeInMs = operationMeasurement {
    arrayA.toSeq
  }
  println(s"Operation [Int]: arrayA toSeq for $fiveMillion processed in: " + durationTimeInMs + " ms")
}
