package com.scala.benchmark.primitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

object IndexOf extends App {
  val testRange1 = 1 to 10
  val testRange2 = 11 to 20
  val test = 1 to 20

  //tenThousand
  val tenThousandRange1 = 1 to 10000
  val tenThousandRange2 = 10001 to 20000
  val tenThousand = 10000
  val tenPercentOfTenThousand = tenThousand / 70

  // oneMillion
  val oneMillionRange1 = 1 to 500000
  val oneMillionRange2 = 500001 to 1000000
  val oneMillion = 1000000

  // fiveMillion
  val fiveMillionRange1 = 1 to 5000000
  val fiveMillionRange2 = 5000001 to 10000000
  val fiveMillion = 5000000
  val tenPercentOfFiveMillion = fiveMillion / 70

  val searchValue = 114793

  //println(List.fill(5)(Random.between(1, 11)))

  ////for (list <-prueba <- testRange1) yield testRange1.indexOf(prueba)
  //println(testRange1.toList)
  //println(testRange2.toList)
  //println(testRange1.toList.appendedAll(testRange2.toList))
  //println(testRange1.toList.prependedAll(testRange2.toList))

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA = tenThousandRange1.toList
  var listB = tenThousandRange2.toList
  var durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    //for (list <-list <- List.fill(tenPercentOfTenThousand)(Random.between(1, tenPercentOfTenThousand))) yield listA.indexOf(list)
    listA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $tenThousand processed in: " + durationTimeInMs + " ms")

  listA = fiveMillionRange1.toList
  listB = fiveMillionRange2.toList
  durationTimeInMs = operationMeasurement {
    //for (list <-list <- List.fill(tenPercentOfTenThousand)(Random.between(1, tenPercentOfTenThousand))) yield listA.indexOf(list)
    listA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA = tenThousandRange1.toVector
  var vectorB = tenThousandRange2.toVector

  durationTimeInMs = operationMeasurement {
    //for (list <-vector <- Vector.fill(tenPercentOfTenThousand)(Random.between(1, tenPercentOfTenThousand))) yield vectorA.indexOf(vector)
    vectorA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $tenThousand processed in: " + durationTimeInMs + " ms")

  vectorA = fiveMillionRange1.toVector
  vectorB = fiveMillionRange2.toVector
  durationTimeInMs = operationMeasurement {
    //for (list <-vector <- Vector.fill(tenPercentOfFiveMillion)(Random.between(1, tenPercentOfFiveMillion))) yield vectorA.indexOf(vector)
    vectorA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA = tenThousandRange1
  var seqB = tenThousandRange2

  durationTimeInMs = operationMeasurement {
    //for (list <-seq <- Seq.fill(tenPercentOfTenThousand)(Random.between(1, tenPercentOfTenThousand))) yield seqA.indexOf(seq)
    seqA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $tenThousand processed in: " + durationTimeInMs + " ms")

  seqA = fiveMillionRange1.toSeq
  seqB = fiveMillionRange2.toSeq
  durationTimeInMs = operationMeasurement {
    //for (list <-seq <- Seq.fill(tenPercentOfFiveMillion)(Random.between(1, tenPercentOfFiveMillion))) yield seqA.indexOf(seq)
    seqA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA = tenThousandRange1.toArray
  var arrayB = tenThousandRange2.toArray

  durationTimeInMs = operationMeasurement {
    //for (list <-array <- Array.fill(tenPercentOfTenThousand)(Random.between(1, tenPercentOfTenThousand))) yield arrayA.indexOf(array)
    arrayA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $tenThousand processed in: " + durationTimeInMs + " ms")

  arrayA = fiveMillionRange1.toArray
  arrayB = fiveMillionRange2.toArray
  durationTimeInMs = operationMeasurement {
    //for (list <-array <- Seq.fill(tenPercentOfFiveMillion)(Random.between(1, tenPercentOfFiveMillion))) yield arrayA.indexOf(array)
    arrayA.indexOf(searchValue)
  }
  println(s"Operation [Int]: indexOf for $fiveMillion processed in: " + durationTimeInMs + " ms")
}
