package com.scala.benchmark.primitives

object ReduceRightOLD extends App {
  val testRange1 = 1 to 10
  val testRange2 = 11 to 20
  val test = 1 to 20

  //tenThousand
  val oneThousandRange1 = 1 to 1000
  val oneThousand = 1000

  //println(testRange1.toList)
  //println(testRange2.toList)

  def operationMeasurement[T](operation: => T): Long = {
    val time = System.currentTimeMillis()
    operation
    System.currentTimeMillis() - time
  }

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA = oneThousandRange1.toList
  var durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    listA.reduceRight(_ + _)
  }
  println(s"Operation [Int]: listA.reduceRight(_ + _) for $oneThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA = oneThousandRange1.toVector

  durationTimeInMs = operationMeasurement {
    vectorA.reduceRight(_ + _)
  }
  println(s"Operation [Int]: vectorA.reduceRight(_ + _) for $oneThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA = oneThousandRange1

  durationTimeInMs = operationMeasurement {
    seqA.reduceRight(_ + _)
  }
  println(s"Operation [Int]: seqA.reduceRight(_ + _) for $oneThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA = oneThousandRange1.toArray

  durationTimeInMs = operationMeasurement {
    arrayA.reduceRight(_ + _)
  }
  println(s"Operation [Int]: arrayA.reduceRight(_ + _) for $oneThousand processed in: " + durationTimeInMs + " ms")

}
