package com.scala.benchmark.primitives
import com.scala.benchmark.utils.ExecutionTime.operationMeasurement
object FoldRight extends App {
  val testRange1 = 1 to 10
  val testRange2 = 11 to 20
  val test = 1 to 20

  //tenThousand
  val tenThousandRange1 = 1 to 10000
  val tenThousandRange2 = 10001 to 20000
  val tenThousand = 10000

  // oneMillion
  val oneMillionRange1 = 1 to 500000
  val oneMillionRange2 = 500001 to 1000000
  val oneMillion = 1000000

  // fiveMillion
  val fiveMillionRange1 = 1 to 5000000
  val fiveMillionRange2 = 5000001 to 10000000
  val fiveMillion = 5000000

  //println(testRange1.toList)
  //println(testRange2.toList)

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA = tenThousandRange1.toList
  var listB = tenThousandRange2.toList
  var durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    listA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: listA.foldRight(0)(_ + _) for $tenThousand processed in: " + durationTimeInMs +" ms")

  listA = oneMillionRange1.toList
  listB = oneMillionRange2.toList
  durationTimeInMs = operationMeasurement {
    listA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: listA.foldRight(0)(_ + _) for $oneMillion processed in: " + durationTimeInMs +" ms")

  listA = fiveMillionRange1.toList
  listB = fiveMillionRange2.toList
  durationTimeInMs = operationMeasurement {
    listA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: listA.foldRight(0)(_ + _) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA = tenThousandRange1.toVector
  var vectorB = tenThousandRange2.toVector

  durationTimeInMs = operationMeasurement {
    vectorA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: vectorA.foldRight(0)(_ + _) for $tenThousand processed in: " + durationTimeInMs +" ms")

  vectorA = oneMillionRange1.toVector
  vectorB = oneMillionRange2.toVector
  durationTimeInMs = operationMeasurement {
    vectorA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: vectorA.foldRight(0)(_ + _) for $oneMillion processed in: " + durationTimeInMs +" ms")

  vectorA = fiveMillionRange1.toVector
  vectorB = fiveMillionRange2.toVector
  durationTimeInMs = operationMeasurement {
    vectorA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: vectorA.foldRight(0)(_ + _) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA = tenThousandRange1
  var seqB = tenThousandRange2

  durationTimeInMs = operationMeasurement {
    seqA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: seqA.foldRight(0)(_ + _) for $tenThousand processed in: " + durationTimeInMs +" ms")

  seqA = oneMillionRange1.toSeq
  seqB = oneMillionRange1.toSeq
  durationTimeInMs = operationMeasurement {
    seqA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: seqA.foldRight(0)(_ + _) for $oneMillion processed in: " + durationTimeInMs +" ms")

  seqA = fiveMillionRange1.toSeq
  seqB = fiveMillionRange2.toSeq
  durationTimeInMs = operationMeasurement {
    seqA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: seqA.foldRight(0)(_ + _) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA = tenThousandRange1.toArray
  var arrayB = tenThousandRange2.toArray

  durationTimeInMs = operationMeasurement {
    arrayA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: arrayA.foldRight(0)(_ + _) for $tenThousand processed in: " + durationTimeInMs +" ms")

  arrayA = oneMillionRange1.toArray
  arrayB = oneMillionRange2.toArray
  durationTimeInMs = operationMeasurement {
    arrayA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: arrayA.foldRight(0)(_ + _) for $oneMillion processed in: " + durationTimeInMs +" ms")

  arrayA = fiveMillionRange1.toArray
  arrayB = fiveMillionRange2.toArray
  durationTimeInMs = operationMeasurement {
    arrayA.foldRight(0)(_ + _)
  }
  println(s"Operation [Int]: arrayA.foldRight(0)(_ + _) for $fiveMillion processed in: " + durationTimeInMs +" ms")
}
