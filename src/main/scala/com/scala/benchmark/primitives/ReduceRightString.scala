package com.scala.benchmark.primitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement
import scala.io.Source

object ReduceRightString extends App {

  var file1TenThousand = Source.fromFile("src/main/resources/Text1.txt").bufferedReader.readLine
  var file2TenThousand = Source.fromFile("src/main/resources/Text2.txt").bufferedReader.readLine
  var file1OneHundredThousand = (file1TenThousand + " ") * 10
  var file2OneHundredThousand = (file2TenThousand + " ") * 10
  //var test1 = Source.fromFile("src/main/resources/Text1c.txt").bufferedReader.readLine
  //var test2 = Source.fromFile("src/main/resources/Text2c.txt").bufferedReader.readLine
  //println(file1)
  //println(file2)
  //test1 *= 2500
  //test2 *= 500
  //println(file1.split(" ").toList)
  //println(file1.split(" ").toList.size)
  //println(file2.split(" ").toList)
  //println(file2.split(" ").toList.size)
  //println(test1.split(" ").toList.reduceLeft(_ + _))
  //println(test1.split(" ").toList.size)


  val tenThousand = 10000
  val oneHundredThousand = 100000

  //file1.toString.split(" ")
  //test1 *= 5
  //test1 *= 5
  //println(test1.toList)
  //println(test2.toList)

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var listA = file1TenThousand.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  var durationTimeInMs = 0L
  durationTimeInMs = operationMeasurement {
    listA.reduceRight(_ + _)
  }
  println(s"Operation [String]: listA.reduceRight(_ + _) for $tenThousand processed in: " + durationTimeInMs + " ms")

  // one million
  listA = file1OneHundredThousand.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  durationTimeInMs = operationMeasurement {
    listA.reduceRight(_ + _)
  }
  println(s"Operation [String]: listA.reduceRight(_ + _) for $oneHundredThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var vectorA = file1TenThousand.split(" ").toVector

  durationTimeInMs = operationMeasurement {
    vectorA.reduceRight(_ + _)
  }
  println(s"Operation [String]: vectorA.reduceRight(_ + _) for $tenThousand processed in: " + durationTimeInMs + " ms")

  vectorA = file1OneHundredThousand.split(" ").toVector
  durationTimeInMs = operationMeasurement {
    vectorA.reduceRight(_ + _)
  }
  println(s"Operation [String]: vectorA.reduceRight(_ + _) for $oneHundredThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */
  //tenThousand
  var seqA = file1TenThousand.split(" ").toSeq

  durationTimeInMs = operationMeasurement {
    seqA.reduceRight(_ + _)
  }
  println(s"Operation [String]: seqA.reduceRight(_ + _) for $tenThousand processed in: " + durationTimeInMs + " ms")

  seqA = file1OneHundredThousand.split(" ").toSeq
  durationTimeInMs = operationMeasurement {
    seqA.reduceRight(_ + _)
  }
  println(s"Operation [String]: seqA.reduceRight(_ + _) for $oneHundredThousand processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */
  //ten thousand
  var arrayA = file1TenThousand.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA.reduceRight(_ + _)
  }
  println(s"Operation [String]: arrayA.reduceRight(_ + _) for $tenThousand processed in: " + durationTimeInMs + " ms")

  // one million
  arrayA = file1OneHundredThousand.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA.reduceRight(_ + _)
  }
  println(s"Operation [String]: arrayA.reduceRight(_ + _) for $oneHundredThousand processed in: " + durationTimeInMs + " ms")
}
