package com.scala.benchmark.primitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement
import scala.io.Source

object ZipString extends App {

  var file1TenThousand = Source.fromFile("src/main/resources/Text1.txt").bufferedReader.readLine
  var file2TenThousand = Source.fromFile("src/main/resources/Text2.txt").bufferedReader.readLine
  var file1OneMillion = (file1TenThousand + " ") * 100
  var file2OneMillion = (file2TenThousand + " ") * 100
  var file1FiveMillion = file1OneMillion * 5
  var file2FiveMillion = file2OneMillion * 5
  //var test1 = Source.fromFile("src/main/resources/Text1c.txt").bufferedReader.readLine
  //var test2 = Source.fromFile("src/main/resources/Text2c.txt").bufferedReader.readLine
  //println(file1)
  //println(file2)
  //test1 *= 2500
  //test2 *= 500
  //println(file1.split(" ").toList)
  //println(file1.split(" ").toList.size)
  //println(file2.split(" ").toList)
  //println(file2.split(" ").toList.size)
  //println(test1.split(" ").toList)
  //println(test1.split(" ").toList.size)


  val tenThousand = 10000
  val oneMillion = 1000000
  val fiveMillion = 5000000

  //file1.toString.split(" ")
  //test1 *= 5
  //test1 *= 5
  //println(test1.toList)
  //println(test2.toList)

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var listA = file1TenThousand.split(" ").toList
  var listB = file2TenThousand.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  var durationTimeInMs = 0L
  durationTimeInMs = operationMeasurement {
    listA zip listB
    //list.map(_ + 1)
  }
  println(s"Operation [String]: listA zip listB for $tenThousand processed in: " + durationTimeInMs + " ms")

  // one million
  listA = file1OneMillion.split(" ").toList
  listB = file2OneMillion.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  durationTimeInMs = operationMeasurement {
    listA zip listB
  }
  println(s"Operation [String]: listA zip listB for $oneMillion processed in: " + durationTimeInMs + " ms")

  // five million
  listA = file1FiveMillion.split(" ").toList
  listB = file2FiveMillion.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  durationTimeInMs = operationMeasurement {
    listA zip listB
  }
  println(s"Operation [String]: listA zip listB for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var vectorA = file1TenThousand.split(" ").toVector
  var vectorB = file2TenThousand.split(" ").toVector

  durationTimeInMs = operationMeasurement {
    vectorA zip vectorB
  }
  println(s"Operation [String]: vectorA zip vectorB for $tenThousand processed in: " + durationTimeInMs + " ms")

  vectorA = file1OneMillion.split(" ").toVector
  vectorB = file1OneMillion.split(" ").toVector
  durationTimeInMs = operationMeasurement {
    vectorA zip vectorB
  }
  println(s"Operation [String]: vectorA zip vectorB for $oneMillion processed in: " + durationTimeInMs + " ms")

  vectorA = file1FiveMillion.split(" ").toVector
  vectorB = file2FiveMillion.split(" ").toVector
  durationTimeInMs = operationMeasurement {
    vectorA zip vectorB
  }
  println(s"Operation [String]: vectorA zip vectorB for $fiveMillion processed in: " + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */
  //tenThousand
  var seqA = file1TenThousand.split(" ").toSeq
  var seqB = file2TenThousand.split(" ").toSeq

  durationTimeInMs = operationMeasurement {
    seqA zip seqB
  }
  println(s"Operation [String]: seqA zip seqB for $tenThousand processed in: " + durationTimeInMs + " ms")

  seqA = file1OneMillion.split(" ").toSeq
  seqB = file2OneMillion.split(" ").toSeq
  durationTimeInMs = operationMeasurement {
    seqA zip seqB
  }
  println(s"Operation [String]: seqA zip seqB for $oneMillion processed in: " + durationTimeInMs + " ms")

  seqA = file1FiveMillion.split(" ").toSeq
  seqB = file2FiveMillion.split(" ").toSeq
  durationTimeInMs = operationMeasurement {
    seqA zip seqB
  }
  println(s"Operation [String]: seqA zip seqB for $fiveMillion processed in: " + durationTimeInMs + " ms")
  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */
  //ten thousand
  var arrayA = file1TenThousand.split(" ").toArray
  var arrayB = file2TenThousand.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA zip arrayB
  }
  println(s"Operation [String]: arrayA zip arrayB for $tenThousand processed in: " + durationTimeInMs + " ms")

  // one million
  arrayA = file1OneMillion.split(" ").toArray
  arrayB = file2OneMillion.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA zip arrayB
  }
  println(s"Operation [String]: arrayA zip arrayB for $oneMillion processed in: " + durationTimeInMs + " ms")

  // five million
  arrayA = file1FiveMillion.split(" ").toArray
  arrayB = file2FiveMillion.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA zip arrayB
  }
  println(s"Operation [String]: arrayA zip arrayB for $fiveMillion processed in: " + durationTimeInMs + " ms")
}
