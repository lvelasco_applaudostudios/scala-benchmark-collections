package com.scala.benchmark.primitives

import scala.io.Source

object AppendString extends App {
  var file1TenThousand = Source.fromFile("src/main/resources/Text1.txt").bufferedReader.readLine
  var file2TenThousand = Source.fromFile("src/main/resources/Text2.txt").bufferedReader.readLine
  var file1FiveMillion = (file1TenThousand + " ") * 500
  var file2FiveMillion = (file2TenThousand + " ") * 500
  //var test1 = Source.fromFile("src/main/resources/Text1c.txt").bufferedReader.readLine
  //var test2 = Source.fromFile("src/main/resources/Text2c.txt").bufferedReader.readLine
  //println(file1)
  //println(file2)
  //test1 *= 2500
  //test2 *= 500
  //println(file1.split(" ").toList)
  //println(file1FiveMillion.split(" ").toList.size)
  //println(file2.split(" ").toList)
  //println(file2.split(" ").toList.size)
  //println(test1.split(" ").toList.appendedAll(test2.split(" ").toList))
  //println(test1.split(" ").toList.size)


  val tenThousand = 10000
  val fiveMillion = 5000000

  //file1.toString.split(" ")
  //test1 *= 5
  //test1 *= 5
  //println(test1.toList)
  //println(test2.toList)

  def operationMeasurement[T](operation: => T): Long = {
    val time = System.currentTimeMillis()
    operation
    //println(operation.toString)
    System.currentTimeMillis() - time
  }

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var listA = file1TenThousand.split(" ").toList
  var listB = file2TenThousand.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  var durationTimeInMs = 0L
  durationTimeInMs = operationMeasurement {
    listA.appendedAll(listB)
  }
  println(s"""Operation [String]: listA.appendedAll(listB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  // one million
  listA = file1FiveMillion.split(" ").toList
  listB = file2FiveMillion.split(" ").toList
  //println(listA.size)
  //println(listB.size)
  durationTimeInMs = operationMeasurement {
    listA.appendedAll(listB)
  }
  println(s"""Operation [String]: listA.appendedAll(listB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  //tenThousand
  var vectorA = file1TenThousand.split(" ").toVector
  var vectorB = file2TenThousand.split(" ").toVector

  durationTimeInMs = operationMeasurement {
    vectorA.appendedAll(vectorB)
  }
  println(s"""Operation [String]: vectorA.appendedAll(vectorB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  vectorA = file1FiveMillion.split(" ").toVector
  vectorB = file2FiveMillion.split(" ").toVector
  durationTimeInMs = operationMeasurement {
    vectorA.appendedAll(vectorB)
  }
  println(s"""Operation [String]: vectorA.appendedAll(vectorB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */
  //tenThousand
  var seqA = file1TenThousand.split(" ").toSeq
  var seqB = file2TenThousand.split(" ").toSeq

  durationTimeInMs = operationMeasurement {
    seqA.appendedAll(seqB)
  }
  println(s"""Operation [String]: seqA.appendedAll(seqB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  seqA = file1FiveMillion.split(" ").toSeq
  seqB = file2FiveMillion.split(" ").toSeq
  durationTimeInMs = operationMeasurement {
    seqA.appendedAll(seqB)
  }
  println(s"""Operation [String]: seqA.appendedAll(seqB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */
  //ten thousand
  var arrayA = file1TenThousand.split(" ").toArray
  var arrayB = file2TenThousand.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA.appendedAll(arrayB)
  }
  println(s"""Operation [String]: arrayA.appendedAll(arrayB) for $tenThousand processed in: """ + durationTimeInMs + " ms")

  // one million
  arrayA = file1FiveMillion.split(" ").toArray
  arrayB = file2FiveMillion.split(" ").toArray
  durationTimeInMs = operationMeasurement {
    arrayA.appendedAll(arrayB)
  }
  println(s"""Operation [String]: arrayA.appendedAll(arrayB) for $fiveMillion processed in: """ + durationTimeInMs + " ms")
}
