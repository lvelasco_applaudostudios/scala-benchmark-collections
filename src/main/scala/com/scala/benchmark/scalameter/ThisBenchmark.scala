package com.scala.benchmark.scalameter

import org.scalameter.{Key, api}
import org.scalameter.api._
import org.scalameter.picklers.Implicits._
object ThisBenchmark extends Bench[Double] { //Bench.Microbenchmark{
  lazy val executor = SeparateJvmsExecutor(
    new Executor.Warmer.Default,
    Aggregator.min,
    new Measurer.IgnoringGC
  )
  lazy val measurer = new Measurer.IgnoringGC
  lazy val reporter = new LoggingReporter[Double]
  lazy val persistor: api.Persistor.None.type = Persistor.None
  override def defaultConfig: Context = Context(
    Key.exec.benchRuns.:=(200), //
    Key.verbose.:=(false), //
    Key.exec.independentSamples.:=(10)
  )
  val size = 5000000
  val gen: Gen[Int] = Gen.range("size")(1, 1, 1)
  //val list1: Gen[List[Int]] = for {g <- gen} yield (1 until size).toList //
  val list1: Gen[List[Int]] = gen.map(e => (1 to size).toList)
  val list2: List[Int] = (size to size * 2).toList
  performance of "List" in {
    measure method "zip" in {
      using(list1)  in {
        l => {
          val result = l.zip(list2)
          //println(result)
          result
        }
      }
    }
  }
}