package com.scala.benchmark.scalameter

import org.scalameter.Bench
import org.scalameter.api.SeparateJvmsExecutor
import org.scalameter.{Key, api}
import org.scalameter.api._
import org.scalameter.picklers.Implicits._

object ThisBenchmark2 extends Bench.OnlineRegressionReport {//Bench[Double] { //Bench.Microbenchmark{
  override lazy val executor = SeparateJvmsExecutor(
    new Executor.Warmer.Default,
    Aggregator.min,
    new Measurer.IgnoringGC
  )
  //override lazy val measurer = new Measurer.IgnoringGC
  override lazy val reporter = new LoggingReporter[Double]
  //override lazy val persistor: api.Persistor.None.type = Persistor.None
  override def defaultConfig: Context = Context(
    Key.exec.benchRuns.:=(200), //
    Key.verbose.:=(false), //
    Key.exec.independentSamples.:=(10) //benchRuns/independentSamples = measurements in each JVM, the best result 10 JVM
  )
  val size = 5000000
  val gen: Gen[Int] = Gen.range("size")(1, 1, 1)
  //val list1: Gen[List[Int]] = for {g <- gen} yield (1 until size).toList //
  val list1: Gen[List[Int]] = gen.map(e => (1 to size).toList)
  val list2: List[Int] = (size to size * 2).toList
  performance of "List" in {
    measure method "zip" in {
      using(list1)  in {
        l => {
          val result = l.zip(list2)
          //println(result)
          result
        }
      }
    }
  }
}