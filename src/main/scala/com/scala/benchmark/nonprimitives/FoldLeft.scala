package com.scala.benchmark.nonprimitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

import scala.collection.mutable.ListBuffer

object FoldLeft extends App{

  var listBuffer1=new ListBuffer[Transaction]()
  var listBuffer2=new ListBuffer[Transaction]()
  var size=10000
  for(i <- 1 to size){
    listBuffer1=listBuffer1+=Transaction(i,"User "+i,i+1)
    listBuffer2=listBuffer2+=Transaction(i+size,"User "+size,i+size+1)
  }

  var listBuffer1_1=new ListBuffer[Transaction]()
  var listBuffer2_1=new ListBuffer[Transaction]()
  var size_1=1000000
  for(i <- 1 to size){
    listBuffer1_1=listBuffer1_1+=Transaction(i,"User "+i,i+1)
    listBuffer2_1=listBuffer2_1+=Transaction(i+size,"User "+size,i+size+1)
  }

  var listBuffer1_5=new ListBuffer[Transaction]()
  var listBuffer2_5=new ListBuffer[Transaction]()
  var size_5=5000000
  for(i <- 1 to size){
    listBuffer1_5=listBuffer1_5+=Transaction(i,"User "+i,i+1)
    listBuffer2_5=listBuffer2_5+=Transaction(i+size,"User "+size,i+size+1)
  }

  /*listBuffer1 = new ListBuffer[Transaction]()
  listBuffer2 = new ListBuffer[Transaction]()
  size = 10
  for (i <- 1 to size) {
    listBuffer1 = listBuffer1 += Transaction(i, "User " + i, i + 1)
    listBuffer2 = listBuffer2 += Transaction(i + size, "User " + size, i + size + 1)
  }
  println(listBuffer1.foldLeft("")(_ + _))*/

  val tenThousand=10000
  val oneMillion=1000000
  val fiveMillion=5000000

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA=listBuffer1.toList
  var listB=listBuffer2.toList
  var durationTimeInMs=0L

  durationTimeInMs=operationMeasurement{
    //listA.foldLeft("")(_+_)
    listA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: listA foldLeft listB for $tenThousand processed in: "+durationTimeInMs+" ms")

  listA=listBuffer1_1.toList
  listB=listBuffer2_1.toList
  durationTimeInMs=operationMeasurement{
    //listA.foldLeft("")(_+_)
    listA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: listA foldLeft listB for $oneMillion processed in: "+durationTimeInMs+" ms")

  listA=listBuffer1_5.toList
  listB=listBuffer2_5.toList
  durationTimeInMs=operationMeasurement{
    //listA.foldLeft("")(_+_)
    listA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: listA foldLeft listB for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA=listBuffer1.toVector
  var vectorB=listBuffer2.toVector

  durationTimeInMs=operationMeasurement{
    //vectorA.foldLeft("")(_+_)
    vectorA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: vectorA foldLeft vectorB for $tenThousand processed in: "+durationTimeInMs+" ms")

  vectorA=listBuffer1_1.toVector
  vectorB=listBuffer2_1.toVector
  durationTimeInMs=operationMeasurement{
    //vectorA.foldLeft("")(_+_)
    vectorA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: vectorA foldLeft vectorB for $oneMillion processed in: "+durationTimeInMs+" ms")

  vectorA=listBuffer1_5.toVector
  vectorB=listBuffer2_5.toVector
  durationTimeInMs=operationMeasurement{
    //vectorA.foldLeft("")(_+_)
    vectorA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: vectorA foldLeft vectorB for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA=listBuffer1.toSeq
  var seqB=listBuffer2.toSeq

  durationTimeInMs=operationMeasurement{
    //seqA.foldLeft("")(_+_)
    seqA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: seqA foldLeft seqB for $tenThousand processed in: "+durationTimeInMs+" ms")

  seqA=listBuffer1_1.toSeq
  seqB=listBuffer2_1.toSeq
  durationTimeInMs=operationMeasurement{
    //seqA.foldLeft("")(_+_)
    seqA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: seqA foldLeft seqB for $oneMillion processed in: "+durationTimeInMs+" ms")

  seqA=listBuffer1_5.toSeq
  seqB=listBuffer2_5.toSeq
  durationTimeInMs=operationMeasurement{
    //seqA.foldLeft("")(_+_)
    seqA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: seqA foldLeft seqB for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA=listBuffer1.toArray
  var arrayB=listBuffer2.toArray

  durationTimeInMs=operationMeasurement{
    //arrayA.foldLeft("")(_+_)
    arrayA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: arrayA foldLeft arrayB for $tenThousand processed in: "+durationTimeInMs+" ms")

  arrayA=listBuffer1_1.toArray
  arrayB=listBuffer2_1.toArray
  durationTimeInMs=operationMeasurement{
    //arrayA.foldLeft("")(_+_)
    arrayA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: arrayA foldLeft arrayB for $oneMillion processed in: "+durationTimeInMs+" ms")

  arrayA=listBuffer1_5.toArray
  arrayB=listBuffer2_5.toArray
  durationTimeInMs=operationMeasurement{
    //arrayA.foldLeft("")(_+_)
    arrayA.foldLeft(Transaction(0,"",0)) {
      case (a, b) => {
        if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
        else a
      }
    }
  }
  println(s"Operation [Int]: arrayA foldLeft arrayB for $fiveMillion processed in: "+durationTimeInMs+" ms")
}

