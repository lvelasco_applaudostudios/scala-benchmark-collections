package com.scala.benchmark.nonprimitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

import scala.collection.mutable.ListBuffer
import scala.util.Random

object Contains extends App {
  var listBuffer1=new ListBuffer[Transaction]()
  var listBuffer2=new ListBuffer[Transaction]()
  var size=10000
  for(i <- 1 to size){
    listBuffer1=listBuffer1+=Transaction(i,"User "+i,i+1)
    listBuffer2=listBuffer2+=Transaction(i+size,"User "+size,i+size+1)
  }

  var listBuffer1_1=new ListBuffer[Transaction]()
  var listBuffer2_1=new ListBuffer[Transaction]()
  var size_1=1000000
  for(i <- 1 to size){
    listBuffer1_1=listBuffer1_1+=Transaction(i,"User "+i,i+1)
    listBuffer2_1=listBuffer2_1+=Transaction(i+size,"User "+size,i+size+1)
  }

  var listBuffer1_5=new ListBuffer[Transaction]()
  var listBuffer2_5=new ListBuffer[Transaction]()
  var size_5=5000000
  for(i <- 1 to size){
    listBuffer1_5=listBuffer1_5+=Transaction(i,"User "+i,i+1)
    listBuffer2_5=listBuffer2_5+=Transaction(i+size,"User "+size,i+size+1)
  }

  /*listBuffer1 = new ListBuffer[Transaction]()
  listBuffer2 = new ListBuffer[Transaction]()
  size = 10
  for (i <- 1 to size) {
    listBuffer1 = listBuffer1 += Transaction(i, "User " + i, i + 1)
    //listBuffer2 = listBuffer2 += Transaction(i + size, "User " + size, i + size + 1)
  }
  listBuffer2 = Random.shuffle(listBuffer1)
  println(listBuffer2)
  listBuffer2 = listBuffer2.take(3)
  println(listBuffer2)
  println(listBuffer1)
  for (list <- listBuffer2.toList) yield println(listBuffer1.toList.indexOf(list))*/

  val tenThousand=10000
  val oneMillion=1000000
  val tenPercentOfOneMillion=300000
  val fiveMillion=5000000
  val tenPercentOfFiveMillion=1500000

  val searchValue = Transaction(7003, "User 7003", 7004)

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA=listBuffer1_1.toList
  var listB=listBuffer2_1.toList
  var durationTimeInMs=0L
  /*listBuffer2 = Random.shuffle(listA)
  println(listBuffer2)
  listBuffer2 = listBuffer2.take(3)*/
  durationTimeInMs=operationMeasurement{
    //for(list <- Random.shuffle(listA).take(tenPercentOfOneMillion).toList)yield listA.contains(list)
    listA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $oneMillion processed in: "+durationTimeInMs+" ms")

  listA=listBuffer1_5.toList
  listB=listBuffer2_5.toList
  durationTimeInMs=operationMeasurement{
    //for(list <- Random.shuffle(listA).take(tenPercentOfFiveMillion).toList)yield listA.contains(list)
    listA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA=listBuffer1_1.toVector
  var vectorB=listBuffer2_1.toVector

  /*listBuffer2 = Random.shuffle(listBuffer1_1)
  listBuffer2 = listBuffer2.take(tenPercentOfOneMillion)
  listBuffer2 = listBuffer2.toVector*/

  durationTimeInMs=operationMeasurement{
    //for(vector<-Random.shuffle(vectorA).take(tenPercentOfOneMillion).toVector)yield vectorA.contains(vector)
    vectorA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $oneMillion processed in: "+durationTimeInMs+" ms")

  vectorA=listBuffer1_5.toVector
  vectorB=listBuffer2_5.toVector
  durationTimeInMs=operationMeasurement{
    //for(vector<-Random.shuffle(vectorA).take(tenPercentOfFiveMillion).toVector)yield vectorA.contains(vector)
    vectorA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA=listBuffer1_1.toSeq
  var seqB=listBuffer2_1.toSeq

  durationTimeInMs=operationMeasurement{
    //for(seq<-Random.shuffle(seqA).take(tenPercentOfOneMillion).toSeq)yield seqA.contains(seq)
    seqA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $oneMillion processed in: "+durationTimeInMs+" ms")

  seqA=listBuffer1_5.toSeq
  seqB=listBuffer2_5.toSeq
  durationTimeInMs=operationMeasurement{
    //for(seq<-Random.shuffle(seqA).take(tenPercentOfFiveMillion).toSeq)yield seqA.contains(seq)
    seqA.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $fiveMillion processed in: "+durationTimeInMs+" ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA=listBuffer1_1
  var arrayB=listBuffer2_1
  durationTimeInMs=operationMeasurement{
    //for(array<-Random.shuffle(arrayA).take(tenPercentOfOneMillion).toArray)yield arrayA.toArray.contains(array)
    arrayA.toArray.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $oneMillion processed in: "+durationTimeInMs+" ms")

  arrayA=listBuffer1_5
  arrayB=listBuffer2_5
  durationTimeInMs=operationMeasurement{
    //for(array<-Random.shuffle(arrayA).take(tenPercentOfFiveMillion).toArray)yield arrayA.toArray.contains(array)
    arrayA.toArray.contains(searchValue)
  }
  println(s"Operation [Int]: contains for $fiveMillion processed in: "+durationTimeInMs+" ms")
}
