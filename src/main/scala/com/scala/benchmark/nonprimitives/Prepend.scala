package com.scala.benchmark.nonprimitives

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

import scala.collection.mutable.ListBuffer

object Prepend extends App {
  var listBuffer1 = new ListBuffer[Transaction]()
  var listBuffer2 = new ListBuffer[Transaction]()
  var size = 10000
  for (i <- 1 to size) {
    listBuffer1 = listBuffer1 += Transaction(i, "User " + i, i + 1)
    listBuffer2 = listBuffer2 += Transaction(i + size, "User " + size, i + size + 1)
  }

  var listBuffer1_1 = new ListBuffer[Transaction]()
  var listBuffer2_1 = new ListBuffer[Transaction]()
  var size_1 = 1000000
  for (i <- 1 to size) {
    listBuffer1_1 = listBuffer1_1 += Transaction(i, "User " + i, i + 1)
    listBuffer2_1 = listBuffer2_1 += Transaction(i + size, "User " + size, i + size + 1)
  }

  var listBuffer1_5 = new ListBuffer[Transaction]()
  var listBuffer2_5 = new ListBuffer[Transaction]()
  var size_5 = 5000000
  for (i <- 1 to size) {
    listBuffer1_5 = listBuffer1_5 += Transaction(i, "User " + i, i + 1)
    listBuffer2_5 = listBuffer2_5 += Transaction(i + size, "User " + size, i + size + 1)
  }

  /*listBuffer1 = new ListBuffer[Transaction]()
  listBuffer2 = new ListBuffer[Transaction]()
  size = 10
  for (i <- 1 to size) {
    listBuffer1 = listBuffer1 += Transaction(i, "User " + i, i + 1)
    listBuffer2 = listBuffer2 += Transaction(i + size, "User " + size, i + size + 1)
  }
  println(listBuffer1 ++ listBuffer2)*/

  val tenThousand = 10000
  val oneMillion = 1000000
  val fiveMillion = 5000000

  /* ******************************************************************************** */
  /* ********************************* List  *************************************** */
  /* ******************************************************************************** */
  var listA = listBuffer1_1.toList
  var listB = listBuffer2_1.toList
  var durationTimeInMs = 0L

  durationTimeInMs = operationMeasurement {
    //listA.prependedAll(listB)
    listA ++: listB
  }
  println(s"Operation [Int]: listA.prependedAll(listB) for $oneMillion processed in: " + durationTimeInMs +" ms")

  listA = listBuffer1_5.toList
  listB = listBuffer2_5.toList
  durationTimeInMs = operationMeasurement {
    //listA.prependedAll(listB)
    listA ++: listB
  }
  println(s"Operation [Int]: listA.prependedAll(listB) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Vector  ************************************** */
  /* ******************************************************************************** */

  var vectorA = listBuffer1_1.toVector
  var vectorB = listBuffer2_1.toVector

  durationTimeInMs = operationMeasurement {
    //listA.prependedAll(listB)
    vectorA ++: vectorB
  }
  println(s"Operation [Int]: vectorA.prependedAll(vectorB) for $oneMillion processed in: " + durationTimeInMs +" ms")

  vectorA = listBuffer1_5.toVector
  vectorB = listBuffer2_5.toVector
  durationTimeInMs = operationMeasurement {
    //vectorA.prependedAll(vectorB)
    vectorB ++: vectorB
  }
  println(s"Operation [Int]: vectorA.prependedAll(vectorB) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Seq  ************************************** */
  /* ******************************************************************************** */

  var seqA = listBuffer1_1.toSeq
  var seqB = listBuffer2_1.toSeq

  durationTimeInMs = operationMeasurement {
    //seqA.prependedAll(seqB)
    seqA ++: seqB
  }
  println(s"Operation [Int]: seqA.prependedAll(seqB) for $oneMillion processed in: " + durationTimeInMs +" ms")

  seqA = listBuffer1_5.toSeq
  seqB = listBuffer2_5.toSeq
  durationTimeInMs = operationMeasurement {
    //seqA.prependedAll(seqB)
    seqA ++: seqB
  }
  println(s"Operation [Int]: seqA.prependedAll(seqB) for $fiveMillion processed in: " + durationTimeInMs +" ms")

  /* ******************************************************************************** */
  /* ********************************* Array  ************************************** */
  /* ******************************************************************************** */

  var arrayA = listBuffer1_1.toArray
  var arrayB = listBuffer2_1.toArray

  durationTimeInMs = operationMeasurement {
    //arrayA.prependedAll(arrayB)
    arrayA ++: arrayB
  }
  println(s"Operation [Int]: arrayA.prependedAll(arrayB) for $oneMillion processed in: " + durationTimeInMs +" ms")

  arrayA = listBuffer1_5.toArray
  arrayB = listBuffer2_5.toArray
  durationTimeInMs = operationMeasurement {
    //arrayA.prependedAll(arrayB)
    arrayA ++: arrayB
  }
  println(s"Operation [Int]: arrayA.prependedAll(arrayB) for $fiveMillion processed in: " + durationTimeInMs +" ms")
}
