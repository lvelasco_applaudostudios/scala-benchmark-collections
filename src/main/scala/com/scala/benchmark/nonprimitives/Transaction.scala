package com.scala.benchmark.nonprimitives

case class Transaction(id: Int, client: String, amount: Double) {
}