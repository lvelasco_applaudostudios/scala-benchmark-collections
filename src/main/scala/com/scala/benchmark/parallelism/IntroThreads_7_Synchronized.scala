package com.scala.benchmark.parallelism

import com.scala.benchmark.parallelism.IntroThreads_6_Bank.BankAccount

object IntroThreads_7_Synchronized extends App {
  // para resolver los problemas del ejercicio anterior tenemos dos opciones:

  // la más poderosa de las dos opciones es .synchronized
  // option #1: use synchronized()
  def buySafe(account: BankAccount, thing: String, price: Int) =
    account.synchronized {
      account.amount -= price
      println("I've bought " + thing)
      println("my account is now " + account)
      // dos o más threads no podrán interferirse al mismo tiempo al evaluar esta expresión
    }

  for (_ <- 1 to 10000) {
  //for (_ <- 1 to 10) {
    val account = new BankAccount(50000)
    val thread1 = new Thread(() => buySafe(account, "shoes", 3000))
    val thread2 = new Thread(() => buySafe(account, "iPhone12", 4000))
    thread1.start()
    thread2.start()
    Thread.sleep(10)
    if (account.amount != 43000) println("AHA: " + account.amount)
    //println()
  }


}
