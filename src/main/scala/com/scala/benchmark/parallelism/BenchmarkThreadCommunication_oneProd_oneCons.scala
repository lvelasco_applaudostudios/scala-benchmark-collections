package com.scala.benchmark.parallelism

import scala.collection.mutable
import scala.util.Random

object BenchmarkThreadCommunication_oneProd_oneCons extends App {

  def prodConsLargeBuffer(): Unit = {
    val time = System.currentTimeMillis()
    val buffer: mutable.Queue[Int] = new mutable.Queue[Int]
    val capacity = 3
    //var i = 0

    val limit = 10000
    //val limit = 1000000
    //val limit = 5000000

    /*
    Resultados:
    Queue capacity 3

    10,000    = 191
    1,000,000 = 3823
    5,000,000 = 21193
     */

    val consumer = new Thread(() => {
      val random = new Random()

      while(true) {
        // necesitamos sincronizarlos porque sino tendremos dos threads CORRIENDO CONCURRENTLY ACCESANDO y modifcando el buffer
        buffer.synchronized {
          while(buffer.isEmpty) {
            //println("[consumer] buffer empty, waiting...")
            buffer.wait()
          }

          // there must be at least ONE value in the buffer
          val x = buffer.dequeue()
          //println("[consumer] consumed " + x)

          // hey producer, there's empty space available, are you lazy?!
          buffer.notify()
        }

        //Thread.sleep(random.nextInt(250))
      }
    })

    val producer = new Thread(() => {
      val random = new Random()
      var i = 0

      while(true) {
        buffer.synchronized {
          if (buffer.size == capacity) {
            //println("[producer] buffer is full, waiting...")
            buffer.wait()
          }

          if(i <= limit) {
            // there must be at least ONE EMPTY SPACE in the buffer
            //println("[producer] producing " + i)
            buffer.enqueue(i)
          }

          // hey consumer, new food for you!
          buffer.notify()
          i += 1

          if(i == limit + 1) {
            println("Time: " + (System.currentTimeMillis() - time))
          }
        }

        //Thread.sleep(random.nextInt(500))
      }
    })

    consumer.start()
    producer.start()
  }

  prodConsLargeBuffer()
  //val buffer = new Queue((1 to 10).toList)
  //println(buffer)
}

/*class Queue[T](content:List[T]) {
  def pop() =  new Queue(content.init)
  def push(element:T) = new Queue(element::content)
  def peek() = content.last

  override def toString() =  "Queue of: " + content.toString
}*/
