package com.scala.benchmark.parallelism

object IntroThreads_9_Exercise1 extends App {
  /**
   * Exercises
   *
   * 1) Construct 50 "inception" threads
   *     Thread1 -> thread2 -> thread3 -> ...
   *     println("hello from thread #3")
   *   in REVERSE ORDER
   *
   */
  def inceptionThreads(maxThreads: Int, i: Int = 1): Thread = new Thread(() => {
    if (i < maxThreads) {
      val newThread = inceptionThreads(maxThreads, i + 1)
      newThread.start()
      newThread.join() // si comento esta línea no podré saber con certeza el orden de ejecución
    }
    println(s"Hello from thread $i")
  })

  inceptionThreads(10).start()


}
