package com.scala.benchmark.parallelism

import java.util.concurrent.Executors

object IntroThreads_3 extends App {
  // thread scheduling depende de varios factores incluyendo el sistema operativo y la jvm implementation
  // los hilos son caros de start y detener así que la solución es de reutilizarlos
  // la solución para esto es crear un pool (executors)


  // executors
  val pool = Executors.newFixedThreadPool(10)
  pool.execute(() => println("something in the thread pool"))
  pool.execute(() => {
    Thread.sleep(1000)
    println("done after 1 second")
  })
   pool.execute(() => {
   Thread.sleep(1000)
   println("almost done")
   Thread.sleep(1000)
   println("done after 2 seconds")
 })

  pool.shutdown() // no admitirá más acciones aunque después de ejecutado este comando
  //pool.execute(() => println("should not appear")) // throws an exception in the calling thread

  // este interrumpe lo que sea aunque esté a medias y hace que los pool de arriba se detengan y causan error
  // pool.shutdownNow()

  println(pool.isShutdown) // true

}
