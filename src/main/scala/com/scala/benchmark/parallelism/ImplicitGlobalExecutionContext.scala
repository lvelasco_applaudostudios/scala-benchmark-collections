package com.scala.benchmark.parallelism
import java.time.LocalDateTime
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object ImplicitGlobalExecutionContext extends App {

  printInfo("Start Program")
  val task: Future[Unit] = taskHello
  printInfo("Continue")
  Await.result(task, Duration.Inf)

  /*
  esto está siendo ejecutado en el execution context del global execution context de Scala
   */
  def taskHello: Future[Unit] = {
    Future{
      printInfo("Starting Task-Hello")
      println("Hello")
      printInfo("Finished Task-Hello")
    }
  }

  def printInfo(txt: String): Unit = {
    val thread = Thread.currentThread.getName
    println(s"${LocalDateTime.now} [$thread] $txt")
  }

  /*
  La salida es:
  2021-05-18T18:03:43.048 [main] Start Program
  2021-05-18T18:03:43.127 [main] Continue
  2021-05-18T18:03:43.127 [scala-execution-context-global-12] Starting Task-Hello
  Hello
  2021-05-18T18:03:43.127 [scala-execution-context-global-12] Finished Task-Hello
   */

  /*
An execution context allows us to separate the business logic from the execution logic. It is responsible for executing computations specifying how the code is executed.
The global executor uses the default executor service which is backed by a Java ForkJoinPool which manages a limited number of threads.
There is a bunch of properties used to configure the parallelism of the ForkJoinPool.
The global execution context sets maxThreads depending to the number of the available cores on the system.
It means when you run a concurrent Futures using global execution context, the performance of your program depends on the number of the processors available to the JVM
*/
}


