package com.scala.benchmark.parallelism

import java.util.concurrent.Executors

object IntroThreads_2 extends App {
  val threadHello = new Thread(() => (1 to 5).foreach(_ => println("hello")))
  val threadGoodbye = new Thread(() => (1 to 5).foreach(_ => println("goodbye")))
  threadHello.start()
  threadGoodbye.start()
  // Diferentes ejecuciones producen diferentes salidas
  // salidas no predecibles
  // De qué manera encadenamos su ejecución y qué sucede si un thread depende de otro?
}
