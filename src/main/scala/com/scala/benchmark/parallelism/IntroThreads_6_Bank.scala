package com.scala.benchmark.parallelism

object IntroThreads_6_Bank extends App {

  class BankAccount(var amount: Int) {
    override def toString: String = "" + amount
  }

  def buy(account: BankAccount, thing: String, price: Int) = {
    account.amount -= price // account.amount = account.amount - price
    //println("I've bought " + thing)
    //println("my account is now " + account)
  }

  for (_ <- 1 to 10000) {
    val account = new BankAccount(50000)
    val thread1 = new Thread(() => buy(account, "shoes", 3000))
    val thread2 = new Thread(() => buy(account, "iPhone12", 4000))
    thread1.start()
    thread2.start()
    Thread.sleep(10)
    if (account.amount != 43000) println("AHA: " + account.amount)
      //println()
  }
  /*
      thread1 (shoes): 50000
        - account = 50000 - 3000 = 47000
      thread2 (iphone): 50000
        - account = 50000 - 4000 = 46000 overwrites the memory of account.amount
     */

}
