package com.scala.benchmark.parallelism

import scala.collection.mutable
import scala.util.Random

object BenchmarkThreadCommunication_nProd_nCons extends App {

  class Consumer(id: Int, buffer: mutable.Queue[Int]) extends Thread {
    override def run(): Unit = {
      val random = new Random()

      while(true) {
        buffer.synchronized {
          /*
            producer produces value, two Consumers are waiting
            notifies ONE consumer, notifies on buffer
            notifies the other consumer

           */
          while (buffer.isEmpty) {
            //println(s"[consumer $id] buffer empty, waiting...")
            buffer.wait()
          }

          // there must be at least ONE value in the buffer
          val x = buffer.dequeue() // OOps.!
          println(s"[consumer $id] consumed " + x)

          //buffer.notify() //recordar que esta línea sólo avisa a un thread pero necesitaremos avisarles a todos de que pueden continuar
          buffer.notifyAll()
        }

        //Thread.sleep(random.nextInt(250))
        Thread.sleep(100)
      }
    }
  }

  class Producer(id: Int, buffer: mutable.Queue[Int], capacity: Int, limit: Int) extends Thread {
    override def run(): Unit = {
      val time = System.currentTimeMillis()
      val random = new Random()
      var i = 0
      //val limit = 100

      while(i <= limit) {
        buffer.synchronized {
          while (buffer.size == capacity) {
            println(s"[producer $id] buffer is full, waiting...")
            buffer.wait()
          }

          if(i <= limit) {
            println(s"[producer $id] producing " + i)
            buffer.enqueue(i)
          }

          buffer.notifyAll() /* ocupar notify o notifyAll no cambiará porque todos están sincronizados en buffer */
          i += 1


        }

        //Thread.sleep(random.nextInt(500))
      }
      //if(i == limit + 1) {
        println("Time: " + (System.currentTimeMillis() - time))
      //}
    }
  }

  def multiProdCons(nConsumers: Int, nProducers: Int): Unit = {
    val buffer: mutable.Queue[Int] = new mutable.Queue[Int]
    val capacity = 3

    val limit = 5000
    //val limit = 10000
    //val limit = 1000000
    //val limit = 5000000


    (1 to nConsumers).foreach(i => new Consumer(i, buffer).start())
    (1 to nProducers).foreach(i => new Producer(i, buffer, capacity, limit).start())
  }

  multiProdCons(3, 2)

  /*
    Resultados:
    Queue capacity 3

    10,000    = 118
    1,000,000 = 8081
    5,000,000 = 39838
  */

  /*
  1 Prod and 3 Cons
  10,000 = 364,273 * 3 = 1,092,819

  1 Prod and 1 Cons
  10,000 = 1,094,317

  2 Prod          and 3 Cons
  5,000 & 5,000  = 364070

   */

}
