package com.scala.benchmark.parallelism

import com.scala.benchmark.utils.ExecutionTime.{operationMeasurement, printThreadInfo}

import java.util.concurrent.Executors

object BenchmarkFixedThreadPool extends App {

  val ten = 1 to 10
  val tenThousand = 1 to 10000
  val oneMillion = 1 to 1000000
  val fiveMillion = 1 to 5000000
  val tenMillion = 1 to 10000000

  //val list = ten.toList
  val list1 = tenThousand.toList
  val list2 = oneMillion.toList
  val list3 = fiveMillion.toList
  val list4 = tenMillion.toList

  val vector1 = tenThousand.toVector
  val vector2 = oneMillion.toVector
  val vector3 = fiveMillion.toVector

  val seq1 = tenThousand.toSeq
  val seq2 = oneMillion.toSeq
  val seq3 = fiveMillion.toSeq

  val array1 = tenThousand.toArray
  val array2 = oneMillion.toArray
  val array3 = fiveMillion.toArray

  //val list = tenMillion.toList

  /*
  Resultados: list.map(_ + 1)

  newFixedThreadPool: 10,000       =  3 ms

  newFixedThreadPool: 1,0000,000   =  29 ms

  newFixedThreadPool: 5,0000,000   = 1695 ms

  newFixedThreadPool: 10,0000,000  = 1710 ms
   */

  /*
  newFixedThreadPool(3)

  10,000    = 54
  1,000,000 = 1709
  5,000,000 = 14777
  */


  val searchValue = 5000
  def f(x:Int) = List(x-1, x, x+1)

  // executors
  val pool = Executors.newFixedThreadPool(3)

  printThreadInfo("Starting")

  pool.execute(() => {
    printThreadInfo("Begin")
    val duracionEnMs = operationMeasurement {
      //list1.map(_ + 1)
      list1.map(x => f(x))
      vector1.flatMap(x => f(x))
      seq1.indexOf(searchValue)
      array1.reduceLeft((x, y) => x + y)
    }
    printThreadInfo("Tiempo en ms: " + duracionEnMs)
    printThreadInfo("Ended")
  })

  pool.execute(() => {
    printThreadInfo("Begin")
    val duracionEnMs = operationMeasurement {
      list2.map(x => f(x))
      vector2.flatMap(x => f(x))
      seq2.indexOf(searchValue)
      array2.reduceLeft((x, y) => x + y)
    }
    printThreadInfo("Tiempo en ms: " + duracionEnMs)
    printThreadInfo("Ended")
  })

  pool.execute(() => {
    printThreadInfo("Begin")
    val duracionEnMs = operationMeasurement {
      list3.map(x => f(x))
      vector3.flatMap(x => f(x))
      seq3.indexOf(searchValue)
      array3.reduceLeft((x, y) => x + y)
    }
    printThreadInfo("Tiempo en ms: " + duracionEnMs)
    printThreadInfo("Ended")
  })

  pool.shutdown() // no admitirá más acciones aunque después de ejecutado este comando
}
