package com.scala.benchmark.parallelism

object IntroThreads_4 extends App {

  def runInParallel = {
    var x = 0

    val thread1 = new Thread(() => {
      x = 1
    })

    val thread2 = new Thread(() => {
      x = 2
    })

    thread1.start()
    thread2.start()
    println(x)
  }

  for (_ <- 1 to 10000) runInParallel
  // race condition

  // para 10,000 registros se da la oportunidad que 1 es asignado a X. A esto le llaman race condition
  // dos hilos están intentando asignar un valor a la misma ubicación en memoria al mismo tiempo
  // esto puede traer bugs y son difíciles de encontrar

}
