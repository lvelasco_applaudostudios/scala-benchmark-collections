package com.scala.benchmark.parallelism

import java.util.concurrent.Executors

object IntroThreads_1 extends App {
  /*
    interface Runnable {
      public void run()
    }
   */

  // Forma más básica de crear un Thread
  val firstThread = new Thread(new Runnable {
    override def run(): Unit = println("Running in parallel")
  })

  firstThread.start() // esto sólo le da la señal a la JVM para comenzar un JVM Thread
  // create a JVM thread => OS thread

  // es necesario ejecutar el start method y no el run.

  /*
  si el código fuera así:
  val runnable = new Runnable {
    override def run(): Unit = println("Running in ...")
  }
  aThread.run() // esto no haría nada en paralelo.
   */

  /*
  hay una diferencia entre una instancia de thread que es donde nosotros ponemos el código y
  un JVM thread que es donde el código de paralelismo es hosteado para ser ejecutado.
   */

  firstThread.join() // bloquea hasta que un thread haya terminado de ejecutarse
  // join es la manera que te aseguras que un thread ha terminado su ejecución

}
