package com.scala.benchmark.parallelism

object IntroThreads_92_Exercise3 extends App {

  /*
    3 sleep fallacy
   */
  var message = ""
  val awesomeThread = new Thread(() => {
    Thread.sleep(1000)
    message = "Scala is awesome"
  })

  message = "Scala sucks"
  awesomeThread.start()
  Thread.sleep(1001)
  //awesomeThread.join() // wait for the awesome thread to join
  println(message)
  /*
    what's the value of message? almost always "Scala is awesome"
    is it guaranteed? NO!
    why? why not?

    (main thread)
      message = "Scala sucks"
      awesomeThread.start()
      sleep() - relieves execution (freeze the execution a la discreción del OS) (dormirá al menos ese tiempo pero podría tomar más)
    (awesome thread)
      sleep() - relieves execution
    (OS gives the CPU to some important thread - takes CPU for more than 2 seconds)
    (OS gives the CPU back  to the MAIN thread)
      println("Scala sucks")
    (OS gives the CPU to awesomethread)
      message = "Scala is awesome"

   */

  // how do we fix this?
  // syncrhonizing does NOT work
}
