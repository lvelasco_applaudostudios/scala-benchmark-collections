package com.scala.benchmark.parallelism


import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

import scala.collection.parallel.CollectionConverters.ImmutableIterableIsParallelizable

object Par extends App {

  val tenThousand = 1 to 10000
  val oneMillion = 1 to 1000000
  val fiveMillion = 1 to 5000000
  val tenMillion = 1 to 10000000

  /*
  Resultados:

  Serial: 10,000      = 3ms
  Par:    10,000      = 99ms

  Serial: 5,0000,000  = 5008ms
  Par:    5,0000,000  = 3164ms

  Serial: 10,0000,000  = 8795ms
  Par:    10,0000,000  = 5059ms
   */

  //val list = tenThousand.toList
  //val list = fiveMillion.toList
  val list = tenMillion.toList

  /*val duracionEnMs = duracionOperacion {
    list.map(_ + 1)
  }
  println("Tiempo en ms: " + duracionEnMs)*/

  val parallelTime = operationMeasurement {
    list.par.map(_ + 1)
  }
  println("Parallel time: " + parallelTime)
}
