package com.scala.benchmark.parallelism

import com.scala.benchmark.utils.ExecutionTime.{operationMeasurement, printThreadInfo}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.{Duration, DurationInt}

object BenchmarkImplicitGlobalExecutionContext extends App {

  val ten = 1 to 10
  val tenThousand = 1 to 10000
  val oneMillion = 1 to 1000000
  val fiveMillion = 1 to 5000000
  val tenMillion = 1 to 10000000

  //val list = ten.toList
  val list1 = tenThousand.toList
  val list2 = oneMillion.toList
  val list3 = fiveMillion.toList

  val vector1 = tenThousand.toVector
  val vector2 = oneMillion.toVector
  val vector3 = fiveMillion.toVector

  val seq1 = tenThousand.toSeq
  val seq2 = oneMillion.toSeq
  val seq3 = fiveMillion.toSeq

  val array1 = tenThousand.toArray
  val array2 = oneMillion.toArray
  val array3 = fiveMillion.toArray

  val searchValue = 5000
  def f(x:Int) = List(x-1, x, x+1)

  /*
  Resultados: list.map(_ + 1)

  Context Implicit Global: 10,000       = 7     ms
  Context Implicit Global: 1,0000,000   = 46    ms
  Context Implicit Global: 5,0000,000   = 117   ms
  Context Implicit Global: 10,0000,000  = 2168  ms

   */

  /*
  Context Implicit Global(3) con multiples operaciones

  10,000    = 48
  1,000,000 = 7258
  5,000,000 = 12010
  */

  printThreadInfo("Start Program")
  val taskA: Future[Unit] = task1
  val taskB: Future[Unit] = task2
  val taskC: Future[Unit] = task3
  printThreadInfo("Continue")
  Await.result(taskA, 2.seconds)
  Await.result(taskB, Duration.Inf)
  Await.result(taskC, Duration.Inf)

  def task1: Future[Unit] = {
    Future{
      printThreadInfo("Starting")
      val duracionEnMs = operationMeasurement {
        //list1.map(_ + 1)
        list1.map(x => f(x))
        vector1.flatMap(x => f(x))
        seq1.indexOf(searchValue)
        array1.reduceLeft((x, y) => x + y)
      }
      printThreadInfo("Tiempo en ms: " + duracionEnMs)
      printThreadInfo("Finished")
    }
  }

  def task2: Future[Unit] = {
    Future{
      printThreadInfo("Starting")
      val duracionEnMs = operationMeasurement {
        list2.map(x => f(x))
        vector2.flatMap(x => f(x))
        seq2.indexOf(searchValue)
        array2.reduceLeft((x, y) => x + y)
      }
      printThreadInfo("Tiempo en ms: " + duracionEnMs)
      printThreadInfo("Finished")
    }
  }

  def task3: Future[Unit] = {
    Future{
      printThreadInfo("Starting")
      val duracionEnMs = operationMeasurement {
        list3.map(x => f(x))
        vector3.flatMap(x => f(x))
        seq3.indexOf(searchValue)
        array3.reduceLeft((x, y) => x + y)
      }
      printThreadInfo("Tiempo en ms: " + duracionEnMs)
      printThreadInfo("Finished")
    }
  }

}
