package com.scala.benchmark.parallelism

object IntroThreads_91_Exercise2 extends App {
  /*
    2
   */
  var x = 0
  val threads = (1 to 100).map(_ => new Thread(() => x += 1))
  threads.foreach(_.start())
  /*
    1) what is the biggest value possible for x? 100
    2) what is the SMALLEST value possible for x? 1

    thread1: x = 0
    thread2: x = 0
      ...
    thread100: x = 0

    for all threads: x = 1 and write it back to x
   */
  //threads.foreach(_.join()) // con esta línea obtendré el valor máximo
  println(x)

}
