package com.scala.benchmark.parallelism

import com.scala.benchmark.utils.ExecutionTime.operationMeasurement

import scala.collection.parallel.CollectionConverters.{ArrayIsParallelizable, ImmutableIterableIsParallelizable}

object ParFold extends App {

  val tenThousand  = 1 to 10000
  val oneMillion   = 1 to 1000000
  val fiveMillion  = 1 to 5000000
  val tenMillion   = 1 to 10000000
  val fiftyMillion = 1 to 50000000

  /*
  Resultados:

  Serial: 10,000      = 6 ms
  Par:    10,000      = 105 ms

  Serial: 5,0000,000  = 101 ms
  Par:    5,0000,000  = 205 ms

  Serial: 10,0000,000  = 2103 ms
  Par:    10,0000,000  = 2158 ms

  Serial: 50,0000,000  = 806 ms
  Par:    50,0000,000  = 768 ms
   */

  //val list = tenThousand.toVector
  val list = fiveMillion.toVector
  //val list = tenMillion.toVector
  //val list = fiftyMillion.toVector

  val duracionEnMs = operationMeasurement {
    println(list.fold(0)(_ + _))
  }
  println("Tiempo en ms: " + duracionEnMs)

  /*val parallelTime = operationMeasurement {
    println(list.par.fold(0)(_ + _))
  }
  println("Parallel time: " + parallelTime)*/
}