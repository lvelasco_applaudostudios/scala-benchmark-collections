package com.scala.benchmark.parallelism

object IntroThreads_8_Volatile extends App {

  // option #2: use @volatile
  // usar esto en val o var significa que todas reads and writes son sincronizadas

  class BankAccount(@volatile var amount: Int) {
    override def toString: String = "" + amount
  }

  def buySafe(account: BankAccount, thing: String, price: Int) = {
      account.amount -= price
      println("I've bought " + thing)
      println("my account is now " + account)
      // dos o más threads no podrán interferirse al mismo tiempo al evaluar esta expresión
  }


  for (_ <- 1 to 10000) {
    //for (_ <- 1 to 10) {
    val account = new BankAccount(50000)
    val thread1 = new Thread(() => buySafe(account, "shoes", 3000))
    val thread2 = new Thread(() => buySafe(account, "iPhone12", 4000))
    thread1.start()
    thread2.start()
    Thread.sleep(10)
    if (account.amount != 43000) println("AHA: " + account.amount)
    //println()
  }
}
