name := "scala-benchmark-collections"
version := "0.1"
scalaVersion := "2.13.5"
resolvers += "Sonatype OSS Snapshots" at
"https://oss.sonatype.org/content/repositories/releases"
libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0"
libraryDependencies += "com.storm-enroute" %% "scalameter" % "0.21"
testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework")
parallelExecution in Test := false
fork := true
outputStrategy := Some(StdoutOutput)
connectInput := true
fork in test := true